#ifndef PTR_LIST_H
#define PTR_LIST_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct PtrEntry;
typedef struct PtrEntry PtrEntry;


struct PtrEntry
{
	PtrEntry* nextEntry;

	void* data;
};

void initPtrEntryAt(PtrEntry* res, void* data);
PtrEntry* initPtrEntry(void* data);

PtrEntry* nextEntry(PtrEntry* en);

int getListLength(PtrEntry* list);
PtrEntry* getElement(PtrEntry* list, int index);
void deleteElement(PtrEntry* list, int index);
void insertElement(PtrEntry* list, int index, PtrEntry* newEntry);

PtrEntry* lastElement(PtrEntry* list);



#endif

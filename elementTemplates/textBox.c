#include "../elementTemplates/textBox.h"

int parseText(const char* str, char*** outputLines)
{
	int i = 0;
	int lines = 1;
	int lineCharCounter = 0;
	char** linesAr;

	/* Obliczanie ilosci linii */ 
	for(i = 0; str[i] != 0; i++)
	{
		if(lineCharCounter >= CHARS_PER_LINE || str[i] == '\n')
		{
			lines++;
			lineCharCounter = 0;

			if(str[i] != '\n') { i--; }
		}
		else
		{
			lineCharCounter++;
		}
	}

	linesAr = (char**)malloc(sizeof(const char*) * lines);
	for(i = 0; i < lines; i++)
	{
		linesAr[i] = malloc(CHARS_PER_LINE + 1);
	}

	/* Parsowanie tekstu */ 
	for(i = 0, lines = 0, lineCharCounter = 0; str[i] != 0; i++)
	{
		if(lineCharCounter < CHARS_PER_LINE && str[i] != '\n')
		{
			linesAr[lines][lineCharCounter++] = str[i];
		}
		else
		{
			linesAr[lines][lineCharCounter] = 0;

			lineCharCounter = 0;
			lines++;

			if(str[i] != '\n') { i--; }
		}
	}

	*outputLines = linesAr;
	
	return lines + 1;
}


void initTextBoxAt(TextBox* res, ClickHandler clickHandler, char* str)
{
	initTemplateAt(&res->temp, false, (PrintFunc)printTextBox, emptyUpdateFunc, (InputHandler)textBoxInputHandler, (ClickHandler)clickHandler);
	
	res->linesLength = parseText(str, &res->lines);
	res->sitesAmount = (res->linesLength / LINES_PER_SITE) + (0 < res->linesLength % LINES_PER_SITE ? 1 : 0);
}

TextBox* initTextBox(ClickHandler clickHandler, char* str)
{
	TextBox* res = (TextBox*)malloc(sizeof(TextBox));
	
	initTextBoxAt(res, clickHandler, str);
	
	return res;
}

void initTextBoxExecutorAt(TextBoxExecutor* res, TextBox* textBox, WINDOW* win)
{
	int linesPerSite;

	initExecutorAt(&res->executor, &textBox->temp, win);
	linesPerSite = calculateLinesAmountPerSite(res);

	textBox->sitesAmount = (textBox->linesLength / linesPerSite) + (0 < textBox->linesLength % linesPerSite ? 1 : 0);
	
	res->currentSiteIndex = 0;
}

TextBoxExecutor* initTextBoxExecutor(TextBox* textBox, WINDOW* win)
{
	TextBoxExecutor* res = (TextBoxExecutor*)malloc(sizeof(TextBoxExecutor));
	
	initTextBoxExecutorAt(res, textBox, win);
	
	return res;
}


int calculateLinesAmountPerSite(TextBoxExecutor* textBoxExecutor)
{
	return getMaxWinY(textBoxExecutor) - 4;
}

int calculateSitesAmount(TextBoxExecutor* textBoxExecutor)
{
	TextBox* textBox = (TextBox*)getTemplate(textBoxExecutor);
	
	int linesPerSite = calculateLinesAmountPerSite(textBoxExecutor);
	
	
	return  (textBox->linesLength / linesPerSite) + (textBox->linesLength % linesPerSite == 0 ? 0 : 1);
}


void printSite(TextBoxExecutor* textBoxExecutor, int siteIndex)
{
	TextBox* textBox = (TextBox*)getTemplate(textBoxExecutor);
	WINDOW* win = getWindow(textBoxExecutor);
	
	int i;
	int maxLinesPerSite = calculateLinesAmountPerSite(textBoxExecutor);
	
	
	for(i = 0; i <  maxLinesPerSite && (siteIndex *  maxLinesPerSite + i) < textBox->linesLength; i++)
	{
		char* line = textBox->lines[i + siteIndex *  maxLinesPerSite];
		wprintw(win, "%s\n", line);
	}
}

void printTextBox(TextBoxExecutor* textBoxExecutor)
{
	WINDOW* win = getWindow(textBoxExecutor);
	TextBox* textBox = (TextBox*)getTemplate(textBoxExecutor);
	
	if(getCleanWindow(textBoxExecutor)) { wclear(win); }
	
	printSite(textBoxExecutor, textBoxExecutor->currentSiteIndex);
	
	wattron(win, A_BOLD);
	wattron(win, COLOR_PAIR(4));
	wprintw(win, "\n[Strona %d/%d]\nStrzalka w lewo/prawo - przewijanie\nKlawisz 'q' - wyjscie", textBoxExecutor->currentSiteIndex + 1, textBox->sitesAmount);
	wattroff(win, A_BOLD);
	wattroff(win, COLOR_PAIR(4));
	
	wrefresh(win);
}

FlagCarrier textBoxInputHandler(TextBoxExecutor* textBoxExecutor)
{
	FlagCarrier res =  initFlagCarrier();
	TextBox* textBox = (TextBox*)getTemplate(textBoxExecutor);
	
	int readChar = getLastReadChar(textBoxExecutor);

	
	setFlag(&res,  IH_OK);
	
   	if(readChar == KEY_RIGHT)
	{
		if(textBoxExecutor->currentSiteIndex + 1 < textBox->sitesAmount)
		{
			textBoxExecutor->currentSiteIndex++;
		}
	}
	else if(readChar == KEY_LEFT)
	{
		if(0 < textBoxExecutor->currentSiteIndex)
		{
			textBoxExecutor->currentSiteIndex--;
		}
	}
	else if(readChar == CTRL('r'))
	{
		setFlag(res, IH_STOP_EXECUTION);
	}
	else
	{
		setFlag(&res,  IH_IGNORED_CHAR);
	}
	
	if(!getFlag(res,  IH_IGNORED_CHAR))
	{
		setFlag(&res,  IH_CALL_UPDATE);
	}

    return res;
}

#ifndef LIST_H
#define LIST_H

#include <stdlib.h>
#include <ncurses.h>
#include <string.h>
#include <math.h>

#include "../elementTemplates/element.h"

struct List;
struct ListExecutor;

typedef struct List List;
typedef struct ListExecutor ListExecutor;


struct List
{
    Template temp;
	
    int rowsLength;
    char** rows;
};

struct ListExecutor
{
    Executor executor;

    int selectedRow;
    int startRow; /* per print */

    void* args;
};

void initListAt(List* res, ClickHandler clickHandler, int rows);
List* initList(ClickHandler clickHandler, int rows);

void initListExecutorAt(ListExecutor* res, List* list, WINDOW* win);
ListExecutor* initListExecutor(List* list, WINDOW* win);

void printList(ListExecutor* listExecutor);
FlagCarrier listInputHandler(ListExecutor* listExecutor);

#endif

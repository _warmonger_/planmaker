#include "../elementTemplates/element.h"

FlagCarrier emptyHandler(Executor* executor)
{
	return IH_OK;
	
}

void emptyUpdateFunc(Executor* executor)
{
	callPrintFunc(executor);
}


void initTemplateAt(Template* temp, int isCursorEnabled, PrintFunc printFunc, UpdateFunc updateFunc, InputHandler inputHandler, ClickHandler clickHandler)
{
	temp->isCursorEnabled = isCursorEnabled;
	temp->printFunc = printFunc;
	temp->updateFunc = updateFunc;
	temp->inputHandler = inputHandler;
	
	temp->clickHandler = clickHandler;

	printw("%d inputHandler: %d\n",	999, inputHandler);
}

Template* initTemplate(int isCursorEnabled, PrintFunc printFunc, UpdateFunc updateFunc, InputHandler inputHandler, ClickHandler clickHandler)
{
	Template* res = (Template*)malloc(sizeof(Template));
	printw("%dxx inputHandler: %d\n",	0, inputHandler);
	initTemplateAt(res, isCursorEnabled, printFunc, updateFunc, inputHandler, clickHandler);
	printw("%dxx inputHandler: %d\n",	1, res->inputHandler);
	return res;
}

void initNewTemplateAt(Template** temp, int isCursorEnabled, PrintFunc printFunc, UpdateFunc updateFunc, InputHandler inputHandler, ClickHandler clickHandler)
{
	*temp = initTemplate(isCursorEnabled, printFunc, updateFunc, inputHandler, clickHandler);
}

void initExecutorAt(Executor* res, Template* temp, WINDOW* win)
{
	res->win = win;
	res->temp = temp;
	
	getmaxyx(res->win, res->maxWinY, res->maxWinX);
	
	res->lastReadChar = 0;
	
	res->cleanWindow = 1;
	
	res->superExecutor = NULL;
}

Executor* initExecutor(Template* temp, WINDOW* win)
{
	Executor* res = (Executor*)malloc(sizeof(Executor));
	
	initExecutorAt(res, temp, win);
	
	return res;
}

void initNewExecutorAt(Executor** executor, Template* temp, WINDOW* win)
{
	*executor = initExecutor(temp, win);
}

FlagCarrier execute(void* executor)
{
	FlagCarrier res =  initFlagCarrier();
	Template* template = getTemplate(executor);
	int* lastReadChar = &((Executor*)executor)->lastReadChar;
	FlagCarrier inputHandlerResult;

	setFlag(&res, IH_OK);

	curs_set(template->isCursorEnabled);
	callPrintFunc(executor);
	
	while((*lastReadChar = wgetch(((Executor*)executor)->win)) != CTRL('r'))
	{
		if(*lastReadChar == CTRL('d'))
		{
			endwin();
			
			exit(1);
		}

		inputHandlerResult = template->inputHandler(executor);
		
		if(!getFlag(inputHandlerResult, IH_OK))
		{
			return inputHandlerResult;
		}
		else if (getFlag(inputHandlerResult, IH_STOP_EXECUTION))
		{
			break;
		}
		else if(getFlag(inputHandlerResult, IH_CALL_UPDATE))
		{
			callUpdateFunc(executor);
		}
	}

	curs_set(0);
	
	return res;
}


PrintFunc getPrintFunc(void* executor)
{
	return ((Executor*)executor)->temp->printFunc;
}

UpdateFunc getUpdateFunc(void* executor)
{
	return ((Executor*)executor)->temp->updateFunc;
}

InputHandler getInputHandler(void* executor)
{
	return ((Executor*)executor)->temp->inputHandler;
}

ClickHandler getClickHandler(void* executor)
{
	return ((Executor*)executor)->temp->clickHandler;
}

void callPrintFunc(void* executor)
{
	getPrintFunc(executor)(executor);
}

void callUpdateFunc(void* executor)
{
	getUpdateFunc(executor)(executor);
}

FlagCarrier callInputHandler(void* executor, int readChar)
{	
	FlagCarrier res =  initFlagCarrier();

	setLastReadChar(executor, readChar);

	res = getInputHandler(executor)(executor);

	if(getFlag(res, IH_CALL_UPDATE))
	{
		callUpdateFunc(executor);

		zeroFlag(&res, IH_CALL_UPDATE);
	}
	
	return res;
}

FlagCarrier callClickHandler(void* executor)
{
	ClickHandler clickHandler = getClickHandler(executor);

	if(clickHandler == NULL) { return 0; }

	return clickHandler(executor);
}



WINDOW* getWindow(void* executor)
{
	return ((Executor*)executor)->win;
}

Template* getTemplate(void* executor)
{
	return ((Executor*)executor)->temp;
}

int getMaxWinY(void* executor)
{
	return ((Executor*)executor)->maxWinY;
}

int getMaxWinX(void* executor)
{
	return ((Executor*)executor)->maxWinX;
}

int getLastReadChar(void* executor)
{
	return ((Executor*)executor)->lastReadChar;
}

char getCleanWindow(void* executor)
{
	return ((Executor*)executor)->cleanWindow;
}

Executor* getSuperExecutor(void* executor)
{
	return ((Executor*)executor)->superExecutor;
}


void setLastReadChar(void* executor, int newLastReadChar)
{
	((Executor*)executor)->lastReadChar = newLastReadChar;
}

void setCleanWindow(void* executor, int newClearWindow)
{
	((Executor*)executor)->cleanWindow = newClearWindow;
}

void setSuperExecutor(void* executor, void* newSuperExecutor)
{
	((Executor*)executor)->superExecutor = newSuperExecutor;
}

void printElement(void* executor)
{
	callPrintFunc(executor);
}

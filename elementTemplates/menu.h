#ifndef MENU_H
#define MENU_H

#include <stdio.h>
#include <ncurses.h>

#include "../elementTemplates/element.h"
#include "../structs.h"
#include "../elementTemplates/list.h"
#include "../elementTemplates/inputBox.h"


struct Menu;
struct MenuExecutor;

typedef struct Menu Menu;
typedef struct MenuExecutor MenuExecutor;


struct Menu
{
	Template temp;
	
	const char* menuDesc;

	int menuOptionsLength;
	char** menuOptions;
};

struct MenuExecutor
{
	Executor executor;
	
	ListExecutor* listExecutor;

	void* args;
};


void initMenuAt(Menu* res, ClickHandler clickHandler, int optionsLength);
Menu* initMenu(ClickHandler clickHandler, int optionsLength);

void initMenuExecutorAt(MenuExecutor* res, Menu* menu, WINDOW* win);
MenuExecutor* initMenuExecutor(Menu* menu, WINDOW* win);

int getSelectedOption(MenuExecutor* menuExecutor);

void printMenu(MenuExecutor* menuExecutor);

FlagCarrier menuInputHandler(MenuExecutor* menuExecutor);

#endif

#include "../elementTemplates/checkBox.h"

char* defaultStates[2] = { "NIE", "TAK" };


CheckBox* initDefaultCheckBox(char* desc)
{
	return initCheckBox(2, (char**)defaultStates, desc);
}

void initCheckBoxAt(CheckBox* res, int statesLength, char** states, char* desc)
{
	initTemplateAt(&res->temp, false, (PrintFunc)printCheckBox, emptyUpdateFunc, (InputHandler)checkBoxInputHandler, NULL);

	res->statesLength = statesLength;
	res->states = states;
	
	res->desc = desc;
}

CheckBox* initCheckBox(int statesLength, char** states, char* desc)
{
	CheckBox* res = (CheckBox*)malloc(sizeof(CheckBox));
	
	initCheckBoxAt(res, statesLength, states, desc);
	
	return res;
}

void initCheckBoxExecutorAt(CheckBoxExecutor* res, CheckBox* checkBox, WINDOW* win)
{
	initExecutorAt(&res->executor, &checkBox->temp, win);
}

CheckBoxExecutor* initCheckBoxExecutor(CheckBox* checkBox, WINDOW* win)
{
	CheckBoxExecutor* res = (CheckBoxExecutor*)malloc(sizeof(CheckBoxExecutor));
	
	initCheckBoxExecutorAt(res, checkBox, win);
	
	return res;
}


void printCheckBox(CheckBoxExecutor* executor)
{
	CheckBox* checkBox = (CheckBox*) getTemplate(executor);
	WINDOW* win = getWindow(executor);	
	
	wclear(win);
	
	if(checkBox->desc != NULL)
	{
		wprintw(win, "%s: ", checkBox->desc);
	}
	
	wprintw(win, "%s\n", checkBox->states[executor->currentState]);
	
	wmove(win, 0, strlen(checkBox->desc) + 2);	
		
	wrefresh(win);
}

FlagCarrier checkBoxInputHandler(CheckBoxExecutor* executor)
{
	FlagCarrier res =  initFlagCarrier();
	
	int readChar = getLastReadChar(executor);


	setFlag(&res,  IH_OK);
	
	if(readChar == KEY_ENTER || readChar == '\n' || readChar == ' ')
	{
		CheckBox* checkBox = (CheckBox*) getTemplate(executor);
		
		executor->currentState = (executor->currentState + 1) % checkBox->statesLength;
		
		setFlag(&res,  IH_CALL_UPDATE);
		
		return res;
	}
	
	setFlag(&res,  IH_IGNORED_CHAR);
	return res;
}

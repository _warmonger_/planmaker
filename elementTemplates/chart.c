#include "../elementTemplates/chart.h"

char*** initDataMemory(int maxCols, int maxRows)
{
	int i;
	char*** res = (char***)malloc(sizeof(char**) * maxCols);
	
	for(i = 0; i < maxCols; i++)
	{
		res[i] = (char**)malloc(sizeof(char*) * maxRows);
	}
	
	return res;
}

void initChartAt(Chart* res, ClickHandler clickHandler, int cols, int rows)
{
	initTemplateAt(&res->temp, false, (PrintFunc)printChart, emptyUpdateFunc, (InputHandler)chartInputHandler, (ClickHandler)clickHandler);
	
	res->maxCol = cols;
	res->maxRow = rows;
	
	res->data = initDataMemory(cols, rows);

	res->delimiter = '|';
}

Chart* initChart(ClickHandler clickHandler, int cols, int rows)
{
	Chart* res = (Chart*)malloc(sizeof(Chart));
	
	initChartAt(res, clickHandler, cols, rows);
	
	return res;
}

void initChartExecutorAt(ChartExecutor* res, Chart* chart, JumpDeterminant jumpDeterminant, HighLightDeterminant highLightDeterminant, WINDOW* win)
{
	initExecutorAt(&res->executor, &chart->temp, win);
	
	res->jumpDeterminant = jumpDeterminant;
	res->highLightDeterminant = highLightDeterminant;

	res->selectedCol = 0;
	res->selectedRow = 0;
}

ChartExecutor* initChartExecutor(Chart* chart, JumpDeterminant jumpDeterminant, HighLightDeterminant highLightDeterminant, WINDOW* win)
{
	ChartExecutor* res = (ChartExecutor*)malloc(sizeof(ChartExecutor));

	initChartExecutorAt(res, chart, jumpDeterminant, highLightDeterminant, win);

	return res;
}

char*** getChartData(ChartExecutor* executor)
{
	return ((Chart*)getTemplate(&executor->executor))->data;
}

FlagCarrier chartInputHandler(ChartExecutor* chartExecutor)
{
	FlagCarrier res =  initFlagCarrier();
	Chart* chart = (Chart*)getTemplate(chartExecutor);
	WINDOW* win = getWindow(&chartExecutor->executor);
	
	int readChar = getLastReadChar(chartExecutor);
	
	setFlag(&res,  IH_OK);
	
	if(readChar == '\n' || readChar == KEY_ENTER || readChar == ' ')
	{
		ClickHandler clickHandler = getClickHandler(chartExecutor);

		if(clickHandler != NULL)
		{
			if(getFlag(clickHandler((Executor*)chartExecutor), IH_CALL_UPDATE))
			{
				callUpdateFunc(chartExecutor);
			}
		}
	}
	else if(chartExecutor->jumpDeterminant == NULL || chartExecutor->jumpDeterminant(chartExecutor, chartExecutor->selectedCol, chartExecutor->selectedRow))
	{
		if(readChar == KEY_UP)
		{
			if(chartExecutor->selectedRow > 0 && (chartExecutor->jumpDeterminant == NULL || chartExecutor->jumpDeterminant(chartExecutor, chartExecutor->selectedCol, chartExecutor->selectedRow - 1)))
			{
				_refreshChartEntry(chartExecutor, chartExecutor->selectedRow - 1, chartExecutor->selectedCol);
				
				chartExecutor->selectedRow--;	
			}
		}
		else if(readChar == KEY_DOWN)
		{
			if(chartExecutor->selectedRow < chart->maxRow - 1 && (chartExecutor->jumpDeterminant == NULL || chartExecutor->jumpDeterminant(chartExecutor, chartExecutor->selectedCol, chartExecutor->selectedRow + 1)))
			{
				_refreshChartEntry(chartExecutor, chartExecutor->selectedRow + 1, chartExecutor->selectedCol);
				
				chartExecutor->selectedRow++;	
			}
		}
		else if(readChar == KEY_LEFT)
		{
			if(chartExecutor->selectedCol > 0 && (chartExecutor->jumpDeterminant == NULL || chartExecutor->jumpDeterminant(chartExecutor, chartExecutor->selectedCol - 1, chartExecutor->selectedRow)))
			{
				_refreshChartEntry(chartExecutor, chartExecutor->selectedRow, chartExecutor->selectedCol - 1);
				
				chartExecutor->selectedCol--;	
			}
		}
		else if(readChar == KEY_RIGHT)
		{
			if(chartExecutor->selectedCol < chart->maxCol - 1 && (chartExecutor->jumpDeterminant == NULL || chartExecutor->jumpDeterminant(chartExecutor, chartExecutor->selectedCol + 1, chartExecutor->selectedRow)))
			{
				_refreshChartEntry(chartExecutor, chartExecutor->selectedRow, chartExecutor->selectedCol + 1);
				
				chartExecutor->selectedCol++;	
			}
		}
		else
		{
			setFlag(&res,  IH_IGNORED_CHAR);
			
			return res;
		}
		
		setFlag(&res,  IH_CALL_UPDATE);
	}
	else
	{
		setFlag(&res,  IH_IGNORED_CHAR);
	}		
	
	return res;
}





void _printChartEntry(ChartExecutor* chartExecutor, int i, int j, int n)
{
	Chart* chart = (Chart*)getTemplate(chartExecutor);
	WINDOW* win = getWindow(chartExecutor);
	
	char*** data = getChartData(chartExecutor);
	
	
	if((chartExecutor->highLightDeterminant == 0 && i == chartExecutor->selectedRow && j == chartExecutor->selectedCol) || (chartExecutor->highLightDeterminant != NULL && chartExecutor->highLightDeterminant(chartExecutor, j, i)))
	{
		wattron(win, COLOR_PAIR(3));
		wprintw(win, "%-*s", n, (data[j][i] == NULL ? "" : (n < strlen(data[j][i]) ? "XXX" : data[j][i])));
		wattroff(win, COLOR_PAIR(3));
	}
	else
	{
		wprintw(win, "%-*s", n, (data[j][i] == NULL ? "" : (n < strlen(data[j][i]) ? "XXX" : data[j][i])));
	}
}

void _refreshChartEntry(ChartExecutor* chartExecutor, int newRow, int newCol)
{
	WINDOW* win = getWindow(chartExecutor);
	
	int n = ((getMaxWinX(chartExecutor) - 1) / ((Chart*)getTemplate(chartExecutor))->maxCol) - 1;	
	int y = 1 + chartExecutor->selectedRow;
	int x = (1 + n) * (chartExecutor->selectedCol + 1) + 1;
	
	
	wmove(win, y, x);
	_printChartEntry(chartExecutor, chartExecutor->selectedRow, chartExecutor->selectedCol, n);
	
	y = 1 + newRow;
	x = (1 + n) * (newCol + 1) + 1;
	
	wmove(win, y, x);
	_printChartEntry(chartExecutor, newRow, newCol, n);
}

void printChart(ChartExecutor* chartExecutor)
{
	int i, j, n;
	Chart* chart = (Chart*)getTemplate(chartExecutor);
	WINDOW* win = getWindow(chartExecutor);
	
		
	n = ((getMaxWinX(chartExecutor) - 1) / chart->maxCol) - 1;
	

	if(getCleanWindow(chartExecutor)) { wclear(win); }
	for(i = 0; i < chart->maxRow; i++)
	{
		wprintw(win, "%c", chart->delimiter);
			
		for(j = 0; j < chart->maxCol; j++)
		{
			_printChartEntry(chartExecutor, i, j, n);
			
			wprintw(win, "%c", chart->delimiter);
		}
			
		wprintw(win, "\n");
	}
	
	if(getCleanWindow(chartExecutor)) { wrefresh(win); }
}

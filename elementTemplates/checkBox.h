#ifndef CHECKBOX_H
#define CHECKBOX_H

#include "../elementTemplates/element.h"

#include <ncurses.h>
#include <stdlib.h>


struct CheckBox;
typedef struct CheckBox CheckBox;

struct CheckBoxExecutor;
typedef struct CheckBoxExecutor CheckBoxExecutor;

struct CheckBox
{
	Template temp;
	
	int statesLength;
	char** states;
	
	char* desc;
};

struct CheckBoxExecutor
{
	Executor executor;

    int currentState;
};

CheckBox* initDefaultCheckBox(char* desc);

void initCheckBoxAt(CheckBox* res, int statesLength, char** states, char* desc);
CheckBox* initCheckBox(int statesLength, char** states, char* desc);

void initCheckBoxExecutorAt(CheckBoxExecutor* res, CheckBox* checkBox, WINDOW* win);
CheckBoxExecutor* initCheckBoxExecutor(CheckBox* checkBox, WINDOW* win);

void printCheckBox(CheckBoxExecutor* executor);
FlagCarrier checkBoxInputHandler(CheckBoxExecutor* executor);

#endif

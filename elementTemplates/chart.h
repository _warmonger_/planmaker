#ifndef CHART_H
#define CHART_H

#include <stdlib.h>
#include <ncurses.h>
#include <string.h>

#include "../elementTemplates/element.h"
#include "../structs.h"


struct Chart;
typedef struct Chart Chart;

struct ChartExecutor;
typedef struct ChartExecutor ChartExecutor;


typedef char (*JumpDeterminant)(ChartExecutor*, int, int);
typedef char (*HighLightDeterminant)(ChartExecutor*, int, int);



struct Chart
{	
	Template temp;
	
	int maxCol, maxRow;
	char*** data; /* Wskaznik do maxCol-elementowej (wskaznik do maxRow-elementowej tablicy) tablicy */

	char delimiter;
};

struct ChartExecutor
{	
	Executor executor;
	
	JumpDeterminant jumpDeterminant;
	HighLightDeterminant highLightDeterminant;
	
	int selectedRow;
	int selectedCol;
};


char*** initDataMemory(int maxCols, int maxRows);

void _refreshChartEntry(ChartExecutor* chartExecutor, int newRow, int newCol);
void _printChartEntry(ChartExecutor* chartExecutor, int i, int j, int n);


void initChartAt(Chart* chart, ClickHandler clickHandler, int cols, int rows);
Chart* initChart(ClickHandler clickHandler, int cols, int rows);

void initChartExecutorAt(ChartExecutor* res, Chart* chart, JumpDeterminant jumpDeterminant, HighLightDeterminant highLightDeterminant, WINDOW* win);
ChartExecutor* initChartExecutor(Chart* chart, JumpDeterminant jumpDeterminant, HighLightDeterminant highLightDeterminant, WINDOW* win);

char*** getChartData(ChartExecutor* executor);

void printChart(ChartExecutor* chartExecutor);
FlagCarrier chartInputHandler(ChartExecutor* chartExecutor);

#endif

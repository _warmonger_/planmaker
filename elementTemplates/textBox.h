#ifndef TEXTBOX_H
#define TEXTBOX_H

#define LINES_PER_SITE 2
#define CHARS_PER_LINE 70

#include <ncurses.h>
#include <stdlib.h>

#include "../elementTemplates/element.h"


struct TextBox
{
	Template temp;
	
	int linesLength;
	char** lines;
	int sitesAmount;
};
typedef struct TextBox TextBox;

struct TextBoxExecutor
{
	Executor executor;
	
	int currentSiteIndex;
};
typedef struct TextBoxExecutor TextBoxExecutor;

int parseText(const char* str, char*** outputLines);

void initTextBoxAt(TextBox* res, ClickHandler clickHandler, char* str);
TextBox* initTextBox(ClickHandler clickHandler, char* str);

void initTextBoxExecutorAt(TextBoxExecutor* res, TextBox* textBox, WINDOW* win);
TextBoxExecutor* initTextBoxExecutor(TextBox* textBox, WINDOW* win);

int calculateLinesAmountPerSite(TextBoxExecutor* textBoxExecutor);
int calculateSitesAmount(TextBoxExecutor* textBoxExecutor);

void printSite(TextBoxExecutor* textBoxExecutor, int siteIndex);
void printTextBox(TextBoxExecutor* textBoxExecutor);

FlagCarrier textBoxInputHandler(TextBoxExecutor* textBoxExecutor);

#endif

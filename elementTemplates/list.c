#include "../elementTemplates/list.h"

void initListAt(List* res, ClickHandler clickHandler, int rows)
{
    int i;
	
    initTemplateAt(&res->temp, false, (PrintFunc)printList, emptyUpdateFunc, (InputHandler)listInputHandler, (ClickHandler)clickHandler);

    res->rowsLength = rows;

    res->rows = (char**)malloc(sizeof(char*) * rows);
    for(i = 0; i < rows; i++)
    {
        res->rows[i] = NULL;
    }
}

List* initList(ClickHandler clickHandler, int rows)
{
    List* res = (List*)malloc(sizeof(List));
	
	initListAt(res, clickHandler, rows);

    return res;
}

ListExecutor* initListExecutor(List* list, WINDOW* win)
{
    int i;
    ListExecutor* res = (ListExecutor*)malloc(sizeof(ListExecutor));

    initExecutorAt(&res->executor, &list->temp, win);
    res->selectedRow = 0;
    res->startRow = 0;

    res->args = NULL;

    return res;
}

void _printListEntry(ListExecutor* listExecutor, int row)
{
	List* list = (List*)getTemplate(listExecutor);
	WINDOW* win = getWindow(listExecutor);
	int numberLen = ((int)log10(list->rowsLength)) + 1;
	
	if(listExecutor->selectedRow == row)
	{
		wattron(win, COLOR_PAIR(3));
		wprintw(win, "%.*d. %s\n", numberLen, row + listExecutor->startRow + 1, list->rows[row + listExecutor->startRow]);
		wattroff(win, COLOR_PAIR(3));
	}
	else
	{
		wprintw(win, "%.*d. %s\n", numberLen, row + listExecutor->startRow + 1, list->rows[row + listExecutor->startRow]);
	}
}

void _refreshListEntry(ListExecutor* listExecutor, int newRow)
{	
	WINDOW* win = getWindow(listExecutor);
	
	wmove(win, listExecutor->selectedRow, 0);
	_printListEntry(listExecutor, listExecutor->selectedRow);
	
	wmove(win, newRow, 0);
	_printListEntry(listExecutor, newRow);
}

void printList(ListExecutor* listExecutor)
{
	List* list = (List*)getTemplate(listExecutor);
	WINDOW* win = getWindow(listExecutor);
	int numberLen = ((int)log10(list->rowsLength)) + 1;
	int i, j;	
	

	if(getCleanWindow(listExecutor)) { wclear(win); }

	for(i = 0; i < getMaxWinY(listExecutor) && i + listExecutor->startRow < list->rowsLength && list->rows[i + listExecutor->startRow] != NULL; i++)
	{
		_printListEntry(listExecutor, i);
	}

	wrefresh(win);	
}

FlagCarrier listInputHandler(ListExecutor* listExecutor)
{
	FlagCarrier res =  initFlagCarrier();
	FlagCarrier clickHandlerRes;
	List* list = (List*)getTemplate(listExecutor);
	WINDOW* win = getWindow(listExecutor);
	
	int readChar = getLastReadChar(listExecutor);
	
	
	setFlag(&res,  IH_OK);
	
        if(readChar == '\n' || readChar == KEY_ENTER)
		{
			clickHandlerRes = callClickHandler(listExecutor);

			if(getFlag(clickHandlerRes, IH_STOP_EXECUTION))
			{
				setFlag(&res, IH_STOP_EXECUTION);
			}
		}
		else if(readChar == KEY_UP)
		{
			if(listExecutor->selectedRow > 0)
			{
				listExecutor->selectedRow--;

				if(!(listExecutor->startRow < listExecutor->selectedRow && listExecutor->selectedRow < listExecutor->startRow + getMaxWinY(listExecutor)))
				{
					listExecutor->startRow = listExecutor->selectedRow;
				}
				else
				{
					
				}
				
				setFlag(&res,  IH_CALL_UPDATE);
			}
		}
		else if(readChar == KEY_DOWN)
		{
			if(listExecutor->selectedRow + 1 < list->rowsLength)
			{
				listExecutor->selectedRow++;

				if(!(listExecutor->startRow <= listExecutor->selectedRow && listExecutor->selectedRow < listExecutor->startRow + getMaxWinY(listExecutor)))
				{
					listExecutor->startRow = listExecutor->selectedRow - getMaxWinY(listExecutor);
				}
				else
				{
					
				}
				
				setFlag(&res,  IH_CALL_UPDATE);
			}
		}
		else
		{
			setFlag(&res,  IH_IGNORED_CHAR);
		}

    return res;
}

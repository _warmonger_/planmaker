#ifndef ELEMENT_H
#define ELEMENT_H

#ifndef CTRL
#define CTRL(c) ((c) & 037)
#endif

/* Kody wykonania obslugi wejscia (makra rozpoczete na "IH"):
	== 0	OK
	> 0		Zakonczono bez bledu
	< 0		Zakonczono z bledem */
#define IH_OK 63
#define IH_IGNORED_CHAR 0
#define IH_STOP_EXECUTION 1
#define IH_CALL_UPDATE 2



#include <stdlib.h>
#include <ncurses.h>

#include "../flag.h"


struct Template;
struct Executor;

typedef struct Template Template;
typedef struct Executor Executor;



typedef FlagCarrier 	(*ClickHandler)	(Executor*);

typedef void 	(*PrintFunc)	(Executor*);
typedef void 	(*UpdateFunc)	(Executor*);
typedef FlagCarrier 	(*InputHandler)	(Executor*);


struct Template
{
	char isCursorEnabled;

	PrintFunc printFunc;
	UpdateFunc updateFunc;
	InputHandler inputHandler;
	
	ClickHandler clickHandler;
};

struct Executor
{
	WINDOW* win;
	Template* temp;
	
	int maxWinY;
	int maxWinX;
	
	int lastReadChar;
	
	char cleanWindow;
	
	Executor* superExecutor;
};


FlagCarrier emptyHandler(Executor* executor);
void emptyUpdateFunc(Executor* executor);


void initTemplateAt		(Template* temp, 	int isCursorEnabled, PrintFunc printFunc, UpdateFunc updateFunc, InputHandler inputHandler, ClickHandler clickHandler);
Template* initTemplate	(					int isCursorEnabled, PrintFunc printFunc, UpdateFunc updateFunc, InputHandler inputHandler, ClickHandler clickHandler);
void initNewTemplateAt	(Template** temp, 	int isCursorEnabled, PrintFunc printFunc, UpdateFunc updateFunc, InputHandler inputHandler, ClickHandler clickHandler);

void initExecutorAt	(Executor* executor, 		Template* temp, WINDOW* win);
Executor* initExecutor	(				Template* temp, WINDOW* win);
void initNewExecutorAt	(Executor** executor, 		Template* temp, WINDOW* win);

PrintFunc getPrintFunc(void* executor);
UpdateFunc getUpdateFunc(void* executor);
InputHandler getInputHandler(void* executor);
ClickHandler getClickHandler(void* executor);

void callPrintFunc(void* executor);
void callUpdateFunc(void* executor);
FlagCarrier callInputHandler(void* executor, int readChar);
FlagCarrier callClickHandler(void* executor);



WINDOW* getWindow(void* executor);
Template* getTemplate(void* executor);
int getMaxWinY(void* executor);
int getMaxWinX(void* executor);
int getLastReadChar(void* executor);
char getCleanWindow(void* executor);
Executor* getSuperExecutor(void* executor);

void setLastReadChar(void* executor, int newLastReadChar);
void setCleanWindow(void* executor, int newClearWindow);
void setSuperExecutor(void* executor, void* newSuperExecutor);



/* Wartosc zwracana:
	== 0		Zakonczono stanem standardowym ( <= Sygnal przerwania od uzytkownika)
	!= 0		Kod wykonania obslugi wejscia */
FlagCarrier execute(void* executor);

void printElement(void* executor);

#endif

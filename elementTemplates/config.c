#include "../elementTemplates/config.h"

void resetCursor(ConfigExecutor* configExecutor)
{
	setCursorAt(&configExecutor->fieldExecutors[configExecutor->currentField], 0);

	wrefresh(getWindow(&configExecutor->fieldExecutors[configExecutor->currentField]));
}

char numberHandler(InputBoxExecutor* executor)
{
	int character = getLastReadChar(executor);

	return character > 47 && character < 58;
}

char letterHandler(InputBoxExecutor* executor)
{
	int character = getLastReadChar(executor);

	return (character > 64 && character < 91) || (character > 96 && character < 123);
}

char* getFieldDesc(Config* config, int index)
{
	return config->fields[index].desc;
}

void setFieldDesc(Config* config, int index, char* fieldDesc)
{
	config->fields[index].desc = fieldDesc;
}

void initConfigAt(Config* res, const char* title, int fieldsLength, SaveConfigFunc saveConfig)
{
	int i;
	
	initTemplateAt(&res->temp, true, (PrintFunc)printConfig, emptyUpdateFunc, (InputHandler)configInputHandler, NULL);
	
	res->title = title;
	res->fieldsLength = fieldsLength;
	res->fields = (InputBox*)malloc(sizeof(InputBox) * fieldsLength);
	
	res->saveConfig = saveConfig;

	for(i = 0; i < fieldsLength; i++)
	{
		initInputBoxAt(&res->fields[i], NULL);
	}
}

Config* initConfig(const char* title, int fieldsLength, SaveConfigFunc saveConfig)
{
	Config* res = (Config*)malloc(sizeof(Config));

	initConfigAt(res, title, fieldsLength, saveConfig);
	
	return res;
}

void initConfigExecutorAt(ConfigExecutor* res, Config* config, WINDOW* win)
{
	int i;
	initExecutorAt(&res->executor, &config->temp, win);
	
	res->fieldExecutors = (InputBoxExecutor*)malloc(sizeof(InputBoxExecutor) * config->fieldsLength);

	for(i = 0; i < config->fieldsLength; i++)
	{
		WINDOW* inputWin = derwin(win, 1, getMaxWinX(win), 2 + i, 0);
		
		initInputBoxExecutorAt(&res->fieldExecutors[i], &config->fields[i], inputWin);
		setSuperExecutor(&res->fieldExecutors[i], res);
	}

	res->currentField = 0;
}

ConfigExecutor* initConfigExecutor(Config* config, WINDOW* win)
{
	ConfigExecutor* res = (ConfigExecutor*)malloc(sizeof(ConfigExecutor));
	
	initConfigExecutorAt(res, config, win);
	
	return res;
}

void printConfig(ConfigExecutor* configExecutor)
{
	Config* config = (Config*)getTemplate(configExecutor);
	WINDOW* win = getWindow(configExecutor);
	
	int i;
	
	
	if(getCleanWindow(configExecutor)) { wclear(win); }

	wmove(win, 0, 0);
	wprintw(win, "%s (wcisnij CTRL+R by wyjsc, CTRL+S by zapisac i wyjsc)\n\n", config->title);

	for(i = 0; i < config->fieldsLength; i++)
	{
		callPrintFunc((Executor*)&configExecutor->fieldExecutors[i]);
	}

	wrefresh(win);

	resetCursor(configExecutor);
}


char goFieldUp(ConfigExecutor* configExecutor)
{
	InputBoxExecutor* iBExecutor;

	if(0 < configExecutor->currentField)
	{
		configExecutor->currentField--;
		
		iBExecutor = &configExecutor->fieldExecutors[configExecutor->currentField];

		resetCursor(configExecutor);

		return 1;
	}

	return 0;
}

char goFieldDown(ConfigExecutor* configExecutor)
{
	Config* config = (Config*)getTemplate(configExecutor);
	
	if(configExecutor->currentField + 1 < config->fieldsLength)
	{
		InputBoxExecutor* iBExecutor = &configExecutor->fieldExecutors[configExecutor->currentField];

		configExecutor->currentField++;

		resetCursor(configExecutor);

		return 1;
	}

	return 0;
}

FlagCarrier configInputHandler(ConfigExecutor* configExecutor)
{
	FlagCarrier res =  initFlagCarrier();
	Config* config = (Config*)getTemplate(configExecutor);
	int readChar = getLastReadChar(configExecutor);
	
	setFlag(&res,  IH_OK);
	
	
	if(readChar == CTRL('s'))
	{
		if(config->saveConfig == NULL || config->saveConfig(configExecutor))
		{
			return res;
		}
		else
		{
			
		}
	}
	else if(readChar == KEY_DOWN || readChar == KEY_ENTER || readChar == '\n')
	{
		if(!goFieldDown(configExecutor))
		{
			setFlag(&res,  IH_IGNORED_CHAR);
		}
	}
	else if(readChar== KEY_UP)
	{
		if(!goFieldUp(configExecutor))
		{
			setFlag(&res,  IH_IGNORED_CHAR);
		}
	}
	/*else
	{
		if(getFlag(callInputHandler(&configExecutor->fieldExecutors[configExecutor->currentField], readChar), IH_CALL_UPDATE))
		{
			callUpdateFunc(&configExecutor->fieldExecutors[configExecutor->currentField]);
		}
		
		return res;
	}*/

	callInputHandler(&configExecutor->fieldExecutors[configExecutor->currentField], readChar);
	callUpdateFunc(&configExecutor->fieldExecutors[configExecutor->currentField]);

	resetCursor(configExecutor);

	return res;
}



void writeValues(ConfigExecutor* configExecutor, FILE* f)
{
	Config* config = (Config*)getTemplate(configExecutor);

	int i;

	
	for(i = 0; i < config->fieldsLength; i++)
	{
		fprintf(f, "%d%s", i, configExecutor->fieldExecutors[i].value);
	}
}

void readValues(ConfigExecutor* configExecutor, FILE* f)
{
	Config* config = (Config*)getTemplate(configExecutor);
	
	int i, index = -1;
	char* str = NULL;
	

	for(i = 0; i < config->fieldsLength; i++)
	{
		fscanf(f, "%d%s", &index, str);

		if(0 <= index && index < config->fieldsLength)
		{
			memcpy(configExecutor->fieldExecutors[index].value, str, strlen(str) + 1);
		}
		else
		{
			fprintf(stderr, "[WARN] Wartosc konfiguracji poza obszarem!\n");
		}
	}
}

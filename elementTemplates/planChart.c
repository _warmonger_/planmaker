#include "../elementTemplates/planChart.h"

char* colStrs[16] = { "GODZINY", "GODZ", "PONIEDZIALEK", "PON", "WTOREK", "WT", "SRODA", "SR", "CZWARTEK", "CZW", "PIATEK", "PT", "SOBOTA", "SOB", "NIEDZIELA", "NIE" };


char planChartHighLightDeterminant(ChartExecutor* cE, int col, int row)
{
	return cE->selectedRow == row && (cE->selectedCol == col || 0 == col);
}

char planChartJumpDeterminant(ChartExecutor* cE, int col, int row)
{
	Chart* chart = (Chart*)getTemplate(cE);

	return 0 < row && row < chart->maxRow && 0 < col && col < chart->maxCol;
}

void initDefaultPlanChartAt(PlanChart* res, ClickHandler clickHandler, int n)
{
	int i;
	Time time;
	Time* period;
	Time* currTime;

	time = mainSettings->startTime;
	period = intToTime(mainSettings->periodLength);
	for(i = 0; i < mainSettings->periodsAmount; i++)
	{
		increaseTimeBy(&time, period);
	}

	res->endHour = time.hour;
	res->endMinute = time.minute;
	res->periodsAmount = mainSettings->periodsAmount;

	initPlanChartAt(res, clickHandler, mainSettings->startTime.hour, mainSettings->startTime.minute, time.hour, time.minute, mainSettings->periodsAmount, n);
}

PlanChart* initDefaultPlanChart(ClickHandler clickHandler, int n)
{
	PlanChart* res = (PlanChart*)malloc(sizeof(PlanChart));
	
	initDefaultPlanChartAt(res, clickHandler, n);
	
	return res;
}

void initPlanChartAt(PlanChart* res, ClickHandler clickHandler, int startHour, int startMinute, int endHour, int endMinute, int periodsAmount, int n)
{
	int i;

	Time *period, *currTime;	
	
	
	initChartAt(&res->rawChart, clickHandler, 8, periodsAmount + 1);
	
	res->startHour = startHour;
	res->startMinute = startMinute;
	res->endHour = endHour;
	res->endMinute = endMinute;
	res->periodsAmount = periodsAmount;
	
	for(i = 1; i < 8; i++)
	{
		res->rawChart.data[i][0] = colStrs[2 * i + ((n < strlen(colStrs[2 * i])) ? 1 : 0)];
	}
	
	period = intToTime((((endHour * 60) + endMinute) - ((startHour * 60) + startMinute)) / periodsAmount);
	currTime = intToTime(startHour * 60 + startMinute);	
	for(i = 1; i < periodsAmount + 1; i++)
	{
		res->rawChart.data[0][i] = (char*)malloc(5 + 1 + 5 + 1);
		
		sprintf(res->rawChart.data[0][i], "%s-", timeToString(currTime));
		increaseTimeBy(currTime, period);
		sprintf(res->rawChart.data[0][i] + 6, "%s", timeToString(currTime));
	}
}

PlanChart* initPlanChart(ClickHandler clickHandler, int startHour, int startMinute, int endHour, int endMinute, int periodsAmount, int n)
{
	PlanChart* res = (PlanChart*)malloc(sizeof(PlanChart));
	
	initPlanChartAt(res, clickHandler, startHour, startMinute, endHour, endMinute, periodsAmount, n);
	
	return res;
}

void initPlanChartExecutorAt(ChartExecutor* res, PlanChart* planChart, WINDOW* win)
{
	initChartExecutorAt(res, &planChart->rawChart, planChartJumpDeterminant, planChartHighLightDeterminant, win);

	res->selectedCol = 1;
	res->selectedRow = 1;
}

ChartExecutor* initPlanChartExecutor(PlanChart* planChart, WINDOW* win)
{
	ChartExecutor* res = (ChartExecutor*)malloc(sizeof(ChartExecutor));
	
	initPlanChartExecutorAt(res, planChart, win);
	
	return res;
}

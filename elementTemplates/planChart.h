#ifndef PLANCHART_H
#define PLANCHART_H


#include <stdlib.h>
#include <ncurses.h>
#include <string.h>

#include "../elementTemplates/element.h"
#include "../structs.h"
#include "../elementTemplates/chart.h"


extern Settings* mainSettings;


struct PlanChart;
typedef struct PlanChart PlanChart;


struct PlanChart
{
	Chart rawChart;
	
	int startHour, startMinute;
	int endHour, endMinute;
	int periodsAmount;
};



char planChartJumpDeterminant(ChartExecutor* chartExecutor, int col, int row);
char planChartHighLightDeterminant(ChartExecutor* chartExecutor, int col, int row);

void initDefaultPlanChartAt(PlanChart* res, ClickHandler clickHandler, int n);
PlanChart* initDefaultPlanChart(ClickHandler clickHandler, int n);
void initPlanChartAt(PlanChart* res, ClickHandler clickHandler, int startHour, int startMinute, int endHour, int endMinute, int periodsAmount, int n);
PlanChart* initPlanChart(ClickHandler clickHandler, int startHour, int startMinute, int endHour, int endMinute, int periodsAmount, int n);

void initPlanChartExecutorAt(ChartExecutor* res, PlanChart* planChart, WINDOW* win);
ChartExecutor* initPlanChartExecutor(PlanChart* planChart, WINDOW* win);

#endif

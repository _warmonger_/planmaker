#ifndef CONFIG_H
#define CONFIG_H


#include <ncurses.h>
#include <stdlib.h>

#include "../elementTemplates/element.h"
#include "../structs.h"
#include "../elementTemplates/inputBox.h"


struct Config;
typedef struct Config Config;

struct ConfigExecutor;
typedef struct ConfigExecutor ConfigExecutor;


typedef char (*SaveConfigFunc)(ConfigExecutor* configExecutor);


#define MAX_VALUE_LENGTH 128


struct Config
{
	Template temp;
	
	const char* title;

	int fieldsLength;
	InputBox* fields;
	
	SaveConfigFunc saveConfig;
};

struct ConfigExecutor
{
	Executor executor;

	InputBoxExecutor* fieldExecutors;
	int currentField;
};

char numberHandler(InputBoxExecutor* executor);
char letterHandler(InputBoxExecutor* executor);

char* getFieldDesc(Config* config, int index);
void setFieldDesc(Config* config, int index, char* fieldDesc);

void initConfigAt(Config* res, const char* title, int fieldsLength, SaveConfigFunc saveConfig);
Config* initConfig(const char* title, int fieldsLength, SaveConfigFunc saveConfig);

void initConfigExecutorAt(ConfigExecutor* res, Config* c, WINDOW* win);
ConfigExecutor* initConfigExecutor(Config* c, WINDOW* win);

void printConfig(ConfigExecutor* configExecutor);
FlagCarrier configInputHandler(ConfigExecutor* configExecutor);


void writeValues(ConfigExecutor* c, FILE* f);
void readValues(ConfigExecutor* c, FILE* f);

#endif

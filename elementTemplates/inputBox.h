#ifndef INPUTBOX_H
#define INPUTBOX_H

#define LINES_PER_SITE 10
#define CHARS_PER_LINE 70


#include <ncurses.h>
#include <stdlib.h>
#include <string.h>

#include "../elementTemplates/element.h"


struct InputBox;
typedef struct InputBox InputBox;

struct InputBoxExecutor;
typedef struct InputBoxExecutor InputBoxExecutor;


struct InputBox
{
	Template temp;
	
	char* desc;
};

struct InputBoxExecutor
{
	Executor executor;

	int lastIndex;
	char* value;

	int maxValuePostDefinedLength;
};

void setCursorAt(InputBoxExecutor* executor, int relY);
int getInputBoxCol(InputBoxExecutor* executor);

void clearInputBox(InputBoxExecutor* executor);

void initInputBoxAt(InputBox* res, char* desc);
InputBox* initInputBox(char* desc);

void initInputBoxExecutorAt(InputBoxExecutor* res, InputBox* inputBox, WINDOW* win);
InputBoxExecutor* initInputBoxExecutor(InputBox* inputBox, WINDOW* win);

void printInputBox(InputBoxExecutor* executor);
FlagCarrier inputBoxInputHandler(InputBoxExecutor* executor);

char* getText(InputBoxExecutor* executor);
void setText(InputBoxExecutor* executor, char* text);

#endif

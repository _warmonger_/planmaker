#include "../elementTemplates/menu.h"

int menuListClickHandler(ListExecutor* listExecutor)
{
	MenuExecutor* menuExecutor = (MenuExecutor*)getSuperExecutor(listExecutor);
	
	return callClickHandler(menuExecutor);
}

void initMenuAt(Menu* res, ClickHandler clickHandler, int optionsLength)
{
    int i;
	
    initTemplateAt(&res->temp, false, (PrintFunc)printMenu, emptyUpdateFunc, (InputHandler)menuInputHandler, (ClickHandler)clickHandler);

    res->menuDesc = NULL;

    res->menuOptionsLength = optionsLength;
    res->menuOptions = (char**)malloc(sizeof(char*) * optionsLength);
    for(i = 0; i < optionsLength; i++)
    {
        res->menuOptions[i] = NULL;
    }
}

Menu* initMenu(ClickHandler clickHandler, int optionsLength)
{
    Menu* res = (Menu*)malloc(sizeof(Menu));
	
	initMenuAt(res, clickHandler, optionsLength);

    return res;
}

void initMenuExecutorAt(MenuExecutor* res, Menu* menu, WINDOW* win)
{
	int i;		
	
	WINDOW* listWin;
	List* optionsList;
	
	initExecutorAt(&res->executor, &menu->temp, win);

	listWin = derwin(win, getMaxWinY(res) - 2, getMaxWinY(res), 2, 0);
	optionsList = initList((ClickHandler)menuListClickHandler, menu->menuOptionsLength);
	for(i = 0; i < menu->menuOptionsLength; i++)
	{
		optionsList->rows[i] = menu->menuOptions[i];
	}
	
	res->listExecutor = initListExecutor(optionsList, listWin);
	setSuperExecutor(res->listExecutor, res);
}

MenuExecutor* initMenuExecutor(Menu* menu, WINDOW* win)
{
	MenuExecutor* res = (MenuExecutor*)malloc(sizeof(MenuExecutor));
	
	initMenuExecutorAt(res, menu, win);

    return res;
}

int getSelectedOption(MenuExecutor* menuExecutor)
{
	return menuExecutor->listExecutor->selectedRow;
}


void printMenu(MenuExecutor* menuExecutor)
{
	WINDOW* win = getWindow(menuExecutor);
	Menu* menu = (Menu*)getTemplate(menuExecutor);
	
	if(getCleanWindow(menuExecutor)) { wclear(win); }

	wprintw(win, "%s\n", menu->menuDesc);
	printList(menuExecutor->listExecutor);

	if(getCleanWindow(menuExecutor)) { wrefresh(win); }
}

FlagCarrier menuInputHandler(MenuExecutor* menuExecutor)
{
	FlagCarrier res = initFlagCarrier();
	int readChar = getLastReadChar(menuExecutor);
	Menu* menu = (Menu*)getTemplate(menuExecutor);

	setFlag(&res,  IH_OK);
	
    if(readChar == KEY_UP || readChar == KEY_DOWN || readChar == KEY_ENTER || readChar == '\n')
    {
		FlagCarrier handlerResult;
		
		handlerResult = callInputHandler(menuExecutor->listExecutor, readChar);		
		if(getFlag(handlerResult, IH_OK))
		{
				setFlag(&res,  IH_CALL_UPDATE);
		}

		if(getFlag(handlerResult, IH_STOP_EXECUTION))
		{
				setFlag(&res,  IH_STOP_EXECUTION);
		}
	
    }
	else
	{
		setFlag(&res,  IH_IGNORED_CHAR);
	}

    return res;
}


#include "../elementTemplates/inputBox.h"

void setCursorAt(InputBoxExecutor* executor, int relY)
{
	WINDOW* win = getWindow(executor);

	wmove(win, relY, getInputBoxCol(executor));

	wrefresh(win);
}

int getInputBoxCol(InputBoxExecutor* executor)
{
	InputBox* inputBox = (InputBox*)getTemplate(executor);

	return (inputBox->desc == NULL ? 0 : (strlen(inputBox->desc) + 2)) + executor->lastIndex;
}

void clearInputBox(InputBoxExecutor* executor)
{
	int i;

	for(i = 0; executor->value[i] != '\0'; i++)
	{
		executor->value[i] = '\0';
	}

	executor->lastIndex = 0;

	callUpdateFunc(executor);
}

void initInputBoxAt(InputBox* res, char* desc)
{
	initTemplateAt(&res->temp, true, (PrintFunc)printInputBox, emptyUpdateFunc, (InputHandler)inputBoxInputHandler, NULL);

	res->desc = desc;
}

InputBox* initInputBox(char* desc)
{
	InputBox* res = (InputBox*)malloc(sizeof(InputBox));
	initInputBoxAt(res, desc);

	return res;
}

void initInputBoxExecutorAt(InputBoxExecutor* res, InputBox* inputBox, WINDOW* win)
{
	initExecutorAt(&res->executor, &inputBox->temp, win);
	
	res->lastIndex = 0;
	res->value = (char*)calloc(sizeof(char) * getMaxWinX(res), 1);

	res->maxValuePostDefinedLength = getMaxWinX(res) - (inputBox->desc == NULL ? 0 : (strlen(inputBox->desc) + 2));

	setCleanWindow(res, true);
}

InputBoxExecutor* initInputBoxExecutor(InputBox* inputBox, WINDOW* win)
{
	InputBoxExecutor* res = (InputBoxExecutor*)malloc(sizeof(InputBoxExecutor));
	initInputBoxExecutorAt(res, inputBox, win);
	
	return res;
}

void printInputBox(InputBoxExecutor* executor)
{
	InputBox* inputBox = (InputBox*)getTemplate(executor);
	WINDOW* win = getWindow(executor);
	
	wclear(win);
	wmove(win, 0, 0);
	if(inputBox->desc != NULL)
	{
		wprintw(win, "%s: ", inputBox->desc);
	}
	
	wprintw(win, "%s", executor->value);
	wmove(win, 0, executor->lastIndex + (inputBox->desc == NULL ? 0 : (strlen(inputBox->desc) + 2)));

	setCursorAt(executor, 0);

	wrefresh(win);
}

FlagCarrier inputBoxInputHandler(InputBoxExecutor* executor)
{
	FlagCarrier res =  initFlagCarrier();
	
	int i;

	int readChar = getLastReadChar(executor);

	
	setFlag(&res,  IH_OK);
	
	if(isprint(readChar))
    	{
		if(executor->lastIndex + 1 < executor->maxValuePostDefinedLength)
		{
			int len = strlen(executor->value);
		    for(i = len; executor->lastIndex < i ; i--)
		    {
			executor->value[i] = executor->value[i - 1];				
    		    }

		    executor->value[executor->lastIndex] = readChar;
		    executor->value[len + 1] = 0;
			executor->lastIndex++;
		}
		else
		{
			setFlag(&res,  IH_IGNORED_CHAR);
		}
	}
	else if(readChar == KEY_BACKSPACE)
    {
        if(executor->lastIndex > 0)
        {
            int i;

            for(i = executor->lastIndex; i < getMaxWinX(executor) - 1 && executor->value[i] != 0; i++)
            {
                executor->value[i - 1] = executor->value[i];
            }

			executor->value[i - 1] = 0;
			executor->lastIndex--;
        }
		else
		{
			setFlag(&res,  IH_IGNORED_CHAR);
		}
    }
/*	else if(readChar == KEY_DL)
    {
        	int len = strlen(executor->value);
		    for(i = executor->lastIndex; i < len ; i++)
		    {
			executor->value[i] = executor->value[i + 1];				
    		    }
    }*/
	else if(readChar == KEY_LEFT)
    {
		if(0 < executor->lastIndex)
		{
			executor->lastIndex--;
		}
		else
		{
			setFlag(&res,  IH_IGNORED_CHAR);
		}
    }
    else if(readChar == KEY_RIGHT)
    {
		if(executor->value[executor->lastIndex] != 0 && executor->lastIndex < getMaxWinX(executor))
		{
			executor->lastIndex++;
		}
		else
		{
			setFlag(&res,  IH_IGNORED_CHAR);
		}
    }
	else
	{
		setFlag(&res,  IH_IGNORED_CHAR);
	}
setFlag(&res,  IH_CALL_UPDATE);
	
	if(!getFlag(res,  IH_IGNORED_CHAR))
	{
		setFlag(&res,  IH_CALL_UPDATE);
	}

    return res;
}

char* getText(InputBoxExecutor* executor)
{
	return executor->value;
}

void setText(InputBoxExecutor* executor, char* text)
{
	InputBox* inputBox = (InputBox*)getTemplate(executor);
	
	int i;
	
	
	for(i = 0; i < executor->maxValuePostDefinedLength && text[i] != 0; i++)
	{
		executor->value[i] = text[i];
	}
	
	executor->value[i] = 0;
	executor->lastIndex = i;
}

/*char inputBoxInputHandler(inputBox* iB, int readChar)
{
    else if(readChar == KEY_DC)
    {
        if(executor->lastIndex < iB->maxValuePostDefinedLength - 1 - 1)
        {
            int i;

            for(i = executor->lastIndex + 1; i < iB->maxValuePostDefinedLength && executor->value[i] != 0; i++)
            {
                executor->value[i - 1] = executor->value[i];
            }

            executor->value[i - 1] = 0;

            return IH_OK;
        }

        return IH_IGNORED_CHAR;
    }
}*/

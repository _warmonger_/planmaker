#include "flag.h"

FlagCarrier* initFlagCarrier()
{
	FlagCarrier* res = (FlagCarrier*)calloc(sizeof(FlagCarrier), 1);

	setFlag(res, IH_OK);
	
	return res;
}

char getFlag(FlagCarrier flagCarrier, int bitNumber)
{
	return (flagCarrier >> bitNumber) % 2;
}

char setFlag(FlagCarrier* flagCarrier, int bitNumber)
{
	*flagCarrier = *flagCarrier | (1 << bitNumber);
}

char zeroFlag(FlagCarrier* flagCarrier, int bitNumber)
{
	*flagCarrier = *flagCarrier & ~(1 << bitNumber);
}

char flipFlag(FlagCarrier* flagCarrier, int bitNumber)
{
	*flagCarrier = *flagCarrier ^ (1 << bitNumber);
}

#include "calc.h"

void cloneAt(Match* res, Match* m)
{	
	res->variantDatas = (VariantData*)malloc(sizeof(VariantData) * m->vDsLength);
	res->vDsLength = m->vDsLength;

	memcpy(res->variantDatas, m->variantDatas, res->vDsLength * sizeof(VariantData));
}

Match* clone(Match* m)
{
	Match* res = malloc(sizeof(Match));
	
	res->variantDatas = (VariantData*)malloc(sizeof(VariantData) * m->vDsLength);
	res->vDsLength = m->vDsLength;

	memcpy(res->variantDatas, m->variantDatas, res->vDsLength * sizeof(VariantData));

	return res;
}

void pushLesson(Match* m, Lesson* l, int variantIndex)
{
	m->variantDatas[m->vDsLength].lesson = l;
	m->variantDatas[m->vDsLength].variantIndex = variantIndex;
	m->vDsLength++;
}

void purgeVariant(Match* m, int variantIndex)
{
	m->vDsLength = variantIndex + 1;
}

char validate(Match* m)
{
	return 0;
}

void computeMatches(Lesson** lessons, int lessonLength, int currentLessonIndex, Match* matches, int maxMatchesLength)
{
	int currentMatchIndex;

	compute(lessons, lessonLength, currentLessonIndex, matches, maxMatchesLength, &currentMatchIndex);
}

void compute(Lesson** lessons, int lessonLength, int currentLessonIndex, Match* matches, int maxMatchesLength, int* currentMatchIndex_)
{
	int i, j;

	Lesson* currLesson = lessons[currentLessonIndex];
	Match* currMatch = &matches[*currentMatchIndex_];

	for(i = 0; i < lessons[currentLessonIndex]->variantsLength; i++)
	{
		pushLesson(currMatch, currLesson, i);

		if(validate(currMatch)) /* Czy aktualne dopasowanie jest prawidlowe */
		{
			if(currentLessonIndex < lessonLength - 1) /* Jesli trzeba dopasowac inne lekcje */
			{
				compute(lessons, lessonLength, currentLessonIndex + 1, matches, maxMatchesLength, currentMatchIndex_);
			}
			else	/* Jesli dopasowanie jest calkowite */					
			{
				(*currentMatchIndex_)++;
				
				cloneAt(&matches[*currentMatchIndex_], currMatch);
			}
		}

		purgeVariant(&matches[*currentMatchIndex_], currentLessonIndex - 1);
	}
}

void computeMatchesAmount(Lesson** lessons, int lessonLength, int currentLessonIndex, Match* currMatch, int* counter)
{
	int i, j;

	Lesson* currLesson = lessons[currentLessonIndex];

	for(i = 0; i < lessons[currentLessonIndex]->variantsLength; i++)
	{
		pushLesson(currMatch, currLesson, i);

		if(validate(currMatch)) /* Czy aktualne dopasowanie jest prawidlowe */
		{
			if(currentLessonIndex < lessonLength - 1) /* Jesli trzeba dopasowac inne lekcje */
			{
				computeMatchesAmount(lessons, lessonLength, currentLessonIndex + 1, currMatch, counter);
			}
			else	/* Jesli dopasowanie jest calkowite */					
			{
				(*counter)++;
			}
		}

		purgeVariant(currMatch, currentLessonIndex - 1);
	}
}

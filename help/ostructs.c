#include "structs.h"

extern settings* mainSettings;
int collide(period* p0, period* p1)
{
	int start0 = p0->start.dayNumber * 24 * 60 + p0->start.hour * 60 + p0->start.minute;
	int end0 = p0->end.dayNumber * 24 * 60 + p0->end.hour * 60 + p0->end.minute;
	int start1 = p1->start.dayNumber * 24 * 60 + p1->start.hour * 60 + p1->start.minute;
	int end1 = p1->end.dayNumber * 24 * 60 + p1->end.hour * 60 + p1->end.minute;
	
	return getCollision(start0, end0, start1, end1);
}

int cmp(const void* i0, const void* i1)
{
	return *((int*)i0) - *((int*)i1);
}

int getCollision(int s0, int e0, int s1, int e1)
{
	int ar[4];  ar[0] = s0, ar[1] = e0, ar[2] = s1, ar[3] = e1;
	
	qsort(ar, sizeof(int), 4, cmp);
		
	if(ar[3] - ar[0] < e1 - s1 + e0 - s0)
	{
		return e1 - s1 + e0 - s0 - ar[3] + ar[0];
	}
		
	return 0;
}

int getDayCode(char* str)
{
	int i;

	for(i = 0; str[i]; i++)
	{
  		str[i] = tolower(str[i]);
	}


	if(strcmp(str, "poniedzialek"))
	{
		return 1;
	}
	else if(strcmp(str, "wtorek"))
	{
		return 2;
	}
	else if(strcmp(str, "sroda"))
	{
		return 3;
	}
	else if(strcmp(str, "czwartek"))
	{
		return 4;
	}
	else if(strcmp(str, "piatek"))
	{
		return 5;
	}
	else if(strcmp(str, "sobota"))
	{
		return 6;
	}
	else if(strcmp(str, "niedziela"))
	{
		return 7;
	}
	else
	{
		return -1;
	}
}

char fprints(FILE* f, const char* str)
{
	fprintf(f, "%ld%s", strlen(str), str);
}

char fscans(FILE* f, char** buff)
{
	int len = -1;
	fscanf(f, "%d", &len);
	
	(*buff) = (char*)malloc(len + 1);
	
	fscanf(f, "%s", (*buff));
	
	return 1;
}

const char* toString(time* t)
{
	char* r = malloc(5);
	
	sprintf((char*)r, "%2d:%2d", t->hour, t->minute);
	
	if(t->hour < 10)
	{
		r[0] = '0';	
	}
	if(t->minute < 10)
	{
		r[3] = '0';	
	}

	return (const char*)r;
}
void increaseBy(time* origin, time* by)
{
	origin->dayNumber += by->dayNumber;
	origin->hour += by->hour;
	origin->minute += by->minute;
	
	while(origin->minute > 59)
	{
		origin->minute -= 60;		
		origin->hour++;
	}
	
	while(origin->hour > 23)
	{
		origin->hour -= 23;		
		origin->dayNumber++;
	}
	
	origin->dayNumber %= 7;
}

time* timeBy(int t)
{
	time* res = (time*)malloc(sizeof(time));
	
	res->minute = t % 60;	t /= 60;
	res->hour = t % 24;		t /= 24;
	res->dayNumber = t % 7;	t /= 7;
	
	return res;
}

time* timeByTime(int dayNumber, int hour, int minute)
{
	time* res = (time*)malloc(sizeof(time));
	
	res->minute = minute;	
	res->hour = hour;	
	res->dayNumber = dayNumber;
	
	return res;
}

int timeToInt(time* t)
{
	return t->dayNumber * 60 * 24 + t->hour * 60 + t->minute;
}

time* intToTime(int i)
{
	time* res = (time*)malloc(sizeof(time));

	res->minute = i % 60;	i /= 60;
	res->hour = i % 60;	i /= 60;
	res->dayNumber = i % 24;

	return res;
}



time* initTime()
{
	time* res = (time*)calloc(sizeof(time), 1);
	
	return res;
}
time* readTime(FILE* f)
{
	time* res = (time*)malloc(sizeof(time));
	
	readTimeInto(f, res);
	
	return res;
}
char readTimeInto(FILE* f, time* res)
{
	int i = 0;

	fscanf(f, "%d%d%d", &res->dayNumber, &res->hour, &res->minute);
	
	return 1;
}
char writeTime(FILE* f, time* t)
{
	fprintf(f, "%d%d%d", t->dayNumber, t->hour, t->minute);
	
	return 1;
}


period* initPeriod()
{
	period* res = (period*)calloc(sizeof(period), 1);
	
	return res;
}
period* readPeriod(FILE* f)
{
	period* res = (period*)malloc(sizeof(period));
	
	readPeriodInto(f, res);
	
	return res;
}
char readPeriodInto(FILE* f, period* res)
{
	if(!readTimeInto(f, &res->start)) { return 0; }
	if(!readTimeInto(f, &res->end)) { return 0; }

	return 1;
}
char writePeriod(FILE* f, period* p)
{
	writeTime(f, &p->start);
	writeTime(f, &p->end);
	
	return 1;
}

void initVariantAt(variant* v)
{
	v->variantCode = "";

	v->periodsLength = 0;
	v->periods = NULL;
}
variant* initVariant()
{
	variant* res = (variant*)calloc(sizeof(variant), 1);
	
	return res;
}
variant* readVariant(FILE* f)
{
	variant* res = (variant*)malloc(sizeof(variant));
	
	readVariantInto(f, res);
	
	return res;
}
char readVariantInto(FILE* f, variant* res)
{
	int i;
	
	fscans(f, &res->variantCode);
	
	fscanf(f, "%d", &res->periodsLength);
	res->periods = (period*)malloc(sizeof(period) * res->periodsLength);
	
	for(i = 0; i < res->periodsLength; i++)
	{
		if(!readPeriodInto(f, &res->periods[i])) { return 0; }	
	}

	return 1;
}
char writeVariant(FILE* f, variant* v)
{
	int i;
	
	fprints(f, v->variantCode);
	
	fprintf(f, "%d", v->periodsLength);
	
	for(i = 0; i < v->periodsLength; i++)
	{
		writePeriod(f, &v->periods[i]);
	}
	
	return 1;
}

lesson* initLesson()
{
	lesson* res = (lesson*)calloc(sizeof(lesson), 1);
	
	return res;
}
lesson* readLesson(FILE* f)
{
	lesson* res = (lesson*)malloc(sizeof(lesson));
	
	readLessonInto(f, res);
	
	return res;
}
char readLessonInto(FILE* f, lesson* res)
{
	int i;

	fscans(f, &res->lessonName);
	

	fscanf(f, "%d", &res->variantsLength);
	res->variants = (variant*)malloc(sizeof(variant) * res->variantsLength);

	for(i = 0; i < res->variantsLength; i++)
	{
		readVariantInto(f, &res->variants[i]);
	}
	
	return 1;
}
char writeLesson(FILE* f, lesson* l)
{
	int i;
	
	fprints(f, l->lessonName);
	
	fprintf(f, "%d", l->variantsLength);
	
	for(i = 0; i < l->variantsLength; i++)
	{
		writeVariant(f, &l->variants[i]);
	}
	
	return 1;
}

PtrEntry* readLessonPack(FILE* f)
{
	int i = 0;
	PtrEntry* res;

	int length = -1;
	fscanf(f, "%d", &length);
	
	if(length == 0) { return res; }

	res = initPtrEntry(NULL);	
	for(i = 0; i + 1 < length; i++)
	{
		res->data = malloc(sizeof(lesson));

		readLessonInto(f, res->data);

		res->nextEntry = initPtrEntry(NULL);
		res = nextEntry(res);
	}

	readLessonInto(f, res->data);

	return res;
}
char writeLessonPack(FILE* f, PtrEntry** ls)
{
	int i = 0;
	int length = getListLength(ls);

	fprintf(f, "%d", length);
	for(i = 0; i < length; i++)
	{
		writeLesson(f, (lesson*)((*ls->data);
	
		ls = nextEntry(ls);
	}

	return 1;
}

settings* initSettings(int lessonsLength)
{
	int i;
	settings* res = (settings*)calloc(sizeof(settings), 1);

	res->lessons = initPtrEntry(NULL);
	
	return res;
}
settings* readSettings(FILE* f)
{
	settings* res = (settings*)malloc(sizeof(settings));
	
	readSettingsInto(f, res);
	
	return res;
}
char readSettingsInto(FILE* f, settings* res)
{
	res->lessons = (lesson**)malloc(sizeof(lesson*));

	*res->lessons = readLessonPack(f);
	
	fscanf(f, "%d%d%d%d%d", &res->maxCollisionLength, &res->maxCollisionAmount, &res->maxGeneratedPlanAmount, &res->periodLength, &res->periodsAmount);
	readTimeInto(f, &res->startTime);
	
	return 1;
}
char writeSettings(FILE* f, settings* s)
{
	writeLessonPack(f, s->lessons);
	
	fprintf(f, "%d%d%d%d%d", s->maxCollisionLength, s->maxCollisionAmount, s->maxGeneratedPlanAmount, s->periodLength, s->periodsAmount);
	readTimeInto(f, &s->startTime);
	
	return 1;
}

match* initMatch(int maxVariantsLength)
{
	match* res = (match*)malloc(sizeof(match));
	
	res->vDsLength = 0;
	res->variantDatas = (variantData*)malloc(sizeof(variantData) * maxVariantsLength);
	
	return res;
}

char addLesson(lesson* l)
{
	lastElement(mainSettings->lessons)->nextEntry = initPtrEntry(l);
		
	return 1;
}

char delLesson(int index)
{
	deleteElement(mainSettings->lessons, index);

	return 1;
	
}

#ifndef STRUCTS_H
#define STRUCTS_H

#define PERIODS_PER_VARIANT 5
#define VARIANTS_PER_LESSON 5
#define VARIANTS_PER_MATCH 5

#define MAX_ARRAY_SIZE 128

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>

#include "ptrList.h"

struct time
{
	int dayNumber;
	int hour;
	int minute;
};
typedef struct time time;


struct period
{
	time start;
	time end;
};
typedef struct period period;


struct variant
{
	char* variantCode;
	
	int periodsLength;
	period* periods;
};
typedef struct variant variant;


struct lesson
{
	char* lessonName;
	
	int variantsLength;
	variant* variants;
};
typedef struct lesson lesson;


struct variantData
{
	lesson* lesson;
	int variantIndex;
};
typedef struct variantData variantData;


struct match
{
	int vDsLength;
	variantData* variantDatas;
};
typedef struct match match;

struct settings
{
	PtrEntry** lessons;

	int maxCollisionLength;		/* MINUTES */
	int maxCollisionAmount;
	int maxGeneratedPlanAmount;

	time startTime;			/* HOUR,MINUTES */
	int periodLength;		/* MINUTES */
	int periodsAmount;
};
typedef struct settings settings;



const char* toString(time* t);

void increaseBy(time* origin, time* by);
time* timeBy(int time);
time* timeByTime(int dayNumber, int hour, int minute);

int timeToInt(time* t);
time* intToTime(int i);

int collide(period* p0, period* p1);
int cmp(const void* i0, const void* i1);
int getCollision(int s0, int e0, int s1, int e1);
int getDayCode(char* str);


time* initTime();
time* readTime(FILE* f);
char readTimeInto(FILE* f, time* res);
char writeTime(FILE* f, time* t);

period* initPeriod();
period* readPeriod(FILE* f);
char readPeriodInto(FILE* f, period* res);
char writePeriod(FILE* f, period* p);

void initVariantAt(variant* v);
variant* initVariant();
variant* readVariant(FILE* f);
char readVariantInto(FILE* f, variant* res);
char writeVariant(FILE* f, variant* v);

lesson* initLesson();
lesson* readLesson(FILE* f);
char readLessonInto(FILE* f, lesson* l);
PtrEntry* readLessonPack(FILE* f);
char writeLesson(FILE* f, lesson* l);
char writeLessonPack(FILE* f, PtrEntry** ls);


settings* initSettings(int lessonsLength);
settings* readSettings(FILE* f);
char readSettingsInto(FILE* f, settings* res);
char writeSettings(FILE* f, settings* l);

match* initMatch(int maxVariantsLength);

char addLesson(lesson* l);
char delLesson(int index);

#endif

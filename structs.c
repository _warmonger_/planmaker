#include "structs.h"

extern Settings* mainSettings;

const char* timeToString(Time* t)
{
	char* r = malloc(5);
	
	sprintf((char*)r, "%2d:%2d", t->hour, t->minute);
	
	if(t->hour < 10)
	{
		r[0] = '0';	
	}
	if(t->minute < 10)
	{
		r[3] = '0';	
	}

	return (const char*)r;
}

int getDayCode(char* str)
{
	int i;

	for(i = 0; str[i]; i++)
	{
  		str[i] = tolower(str[i]);
	}


	if(strcmp(str, "poniedzialek"))
	{
		return 1;
	}
	else if(strcmp(str, "wtorek"))
	{
		return 2;
	}
	else if(strcmp(str, "sroda"))
	{
		return 3;
	}
	else if(strcmp(str, "czwartek"))
	{
		return 4;
	}
	else if(strcmp(str, "piatek"))
	{
		return 5;
	}
	else if(strcmp(str, "sobota"))
	{
		return 6;
	}
	else if(strcmp(str, "niedziela"))
	{
		return 7;
	}
	else
	{
		return -1;
	}
}

int doesCollide(Period* p0, Period* p1)
{
	int buff = 0;
	int least = 1000000;
	int highest = 0;
	
	buff = timeToInt(&p0->start);	
	if(buff < least) { least = buff; }
	if(highest < buff) { highest = buff; }
	
	buff = timeToInt(&p0->end);	
	if(buff < least) { least = buff; }
	if(highest < buff) { highest = buff; }
	
	buff = timeToInt(&p1->start);	
	if(buff < least) { least = buff; }
	if(highest < buff) { highest = buff; }
	
	buff = timeToInt(&p1->end);	
	if(buff < least) { least = buff; }
	if(highest < buff) { highest = buff; }
	
	buff = timeToInt(&p0->end) - timeToInt(&p0->start) + (timeToInt(&p1->end) - timeToInt(&p1->start));
	
	return buff > highest - least ? 0 : (buff - highest + least);
}

int timeToInt(Time* t)
{
	return t->dayNumber * 60 * 24 + t->hour * 60 + t->minute;
}

Time* intToTime(int i)
{
	Time* res = (Time*)malloc(sizeof(Time));

	res->minute = i % 60;	i /= 60;
	res->hour = i % 60;	i /= 60;
	res->dayNumber = i % 24;

	return res;
}

void increaseTimeBy(Time* origin, Time* by)
{
	origin->dayNumber += by->dayNumber;
	origin->hour += by->hour;
	origin->minute += by->minute;
	
	while(origin->minute > 59)
	{
		origin->minute -= 60;		
		origin->hour++;
	}
	
	while(origin->hour > 23)
	{
		origin->hour -= 23;		
		origin->dayNumber++;
	}
	
	origin->dayNumber %= 7;
}

Time* timeByTime(int dayNumber, int hour, int minute)
{
	Time* res = (Time*)malloc(sizeof(Time));
	
	res->minute = minute;	
	res->hour = hour;	
	res->dayNumber = dayNumber;
	
	return res;
}



Time* initTime()
{
	Time* res = (Time*)calloc(sizeof(Time), 1);
	
	return res;
}
Time* readTime(FILE* f)
{
	Time* res = (Time*)malloc(sizeof(Time));
	
	readTimeInto(f, res);
	
	return res;
}

char readTimeInto(FILE* f, Time* res)
{
	int i = 0;

	fscanf(f, "%d %d %d ", &res->dayNumber, &res->hour, &res->minute);
	
	return 1;
}

char writeTime(FILE* f, Time* t)
{
	fprintf(f, "%d %d %d", t->dayNumber, t->hour, t->minute);
	
	return 1;
}


Period* initPeriod()
{
	Period* res = (Period*)calloc(sizeof(Period), 1);
	
	return res;
}
Period* readPeriod(FILE* f)
{
	Period* res = (Period*)malloc(sizeof(Period));
	
	readPeriodInto(f, res);
	
	return res;
}
char readPeriodInto(FILE* f, Period* res)
{
	if(!readTimeInto(f, &res->start)) { return 0; }
	if(!readTimeInto(f, &res->end)) { return 0; }

	return 1;
}
char writePeriod(FILE* f, Period* p)
{
	writeTime(f, &p->start);
	writeTime(f, &p->end);
	
	return 1;
}

void initVariantAt(Variant* v)
{
	v->variantCode = "";

	v->periodsLength = 0;
	v->periods = NULL;
}
Variant* initVariant()
{
	Variant* res = (Variant*)calloc(sizeof(Variant), 1);
	
	return res;
}
Variant* readVariant(FILE* f)
{
	Variant* res = (Variant*)malloc(sizeof(Variant));
	
	readVariantInto(f, res);
	
	return res;
}
char readVariantInto(FILE* f, Variant* res)
{
	int i;
	
	fscanf(f, "%s %d ", res->variantCode, &res->periodsLength);

	for(i = 0; i < res->periodsLength; i++)
	{
		readPeriodInto(f, &res->periods[i]);
	}
	
	return 1;
}
char writeVariant(FILE* f, Variant* v)
{
	int i;
	
	fprintf(f, "%s %d ", v->variantCode, v->periodsLength);
	
	for(i = 0; i < v->periodsLength; i++)
	{
		writePeriod(f, &v->periods[i]);
	}

	return 1;
}

Lesson* initLesson()
{
	Lesson* res = (Lesson*)calloc(sizeof(Lesson), 1);
	
	return res;
}
Lesson* readLesson(FILE* f)
{
	Lesson* res = (Lesson*)malloc(sizeof(Lesson));
	
	readLessonInto(f, res);
	
	return res;
}
char readLessonInto(FILE* f, Lesson* res)
{
	int i;
	
	fscanf(f, "%s %d ", res->lessonName, &res->variantsLength);
	
	res->variants = (Variant*)malloc(sizeof(Variant*) * res->variantsLength);
	
	for(i = 0; i < res->variantsLength; i++)
	{
		readVariantInto(f, &res->variants[i]);
	}
	
	return 1;
}
char writeLesson(FILE* f, Lesson* lesson)
{
	int i;
	
	fprintf(f, "%s %d ", lesson->lessonName, lesson->variantsLength);
	
	for(i = 0; i < lesson->variantsLength; i++)
	{		
		writeVariant(f, (Variant*)&lesson->variants[i]);
	}
	
	return 1;
}

PtrEntry* readLessonPack(FILE* f, int* length)
{
	int i;

	PtrEntry* res = initPtrEntry(NULL);
	PtrEntry* currEntry = res;
	
	
	fscanf(f, "%d ", length);
	
	for(i = 0; i < *length - 1; i++)
	{
		currEntry->data  = readLesson(f);
		
		currEntry->nextEntry = initPtrEntry(NULL);
		currEntry = currEntry->nextEntry;
	}
	
	currEntry->data  = readLesson(f);
	
	return res;
}
char writeLessonPack(FILE* f, PtrEntry* ls)
{
	int i;
	int length = getListLength(ls);
	
	
	fprintf(f, "%d ", length);
	
	if(length == 0) { return 1; }
	
	do
	{
		writeLesson(f, ls->data);
		
		ls = ls->nextEntry;
	}
	while(ls->nextEntry != NULL);
	
	return 1;
}

Settings* initSettings()
{
	int i;
	Settings* res = (Settings*)calloc(sizeof(Settings), 1);

	res->lessons = initPtrEntry(NULL);

	res->maxCollisionLength = 15;
	res->maxCollisionAmount = 2;
	res->maxGeneratedPlanAmount = 1024;

	res->startTime.hour = 8;
	res->startTime.minute = 0;
	res->periodLength = 15;
	res->periodsAmount = 48;
	
	return res;
}
Settings* readSettings(FILE* f)
{
	Settings* res = (Settings*)malloc(sizeof(Settings));
	
	readSettingsInto(f, res);
	
	return res;
}
char readSettingsInto(FILE* f, Settings* res)
{
	res->lessons = readLessonPack(f, &res->lessonsLength);
	
	fscanf(f, "%d %d %d %d %d ", &res->maxCollisionLength, &res->maxCollisionAmount, &res->maxGeneratedPlanAmount, &res->periodLength, &res->periodsAmount);
	
	readTimeInto(f, &res->startTime);
	
	return 1;
}
char writeSettings(FILE* f, Settings* settings)
{
	writeLessonPack(f, settings->lessons);
	
	fprintf(f, "%d %d %d %d %d", settings->maxCollisionLength, settings->maxCollisionAmount, settings->maxGeneratedPlanAmount, settings->periodLength, settings->periodsAmount);

	writeTime(f, &settings->startTime);
	
	return 1;
}

Match* initMatch(int maxVariantsLength)
{
	Match* res = (Match*)malloc(sizeof(Match));
	
	res->vDsLength = 0;
	res->variantDatas = (VariantData*)malloc(sizeof(VariantData) * maxVariantsLength);
	
	return res;
}

char addLesson(Lesson* l)
{
	if(lastElement(mainSettings->lessons) == NULL) { return 0; }
		
	PtrEntry* entry = lastElement(mainSettings->lessons);
	entry->data = l;
	entry->nextEntry = initPtrEntry(NULL);

	mainSettings->lessonsLength++;
		
	return 1;
}

char delLesson(int index)
{
	deleteElement(mainSettings->lessons, index);

	mainSettings->lessonsLength--;

	return 1;
	
}

Lesson** convertLessons()
{
	int i;
	Lesson** res = (Lesson**)malloc(sizeof(Lesson*) * mainSettings->lessonsLength);
	
	PtrEntry* ls = mainSettings->lessons;
	for(i = 0; i < mainSettings->lessonsLength; i++)
	{
		res[i] = ls->data;
		
		ls = ls->nextEntry;
	}
	
	return res;
}

#ifndef PLANGENERATION_H
#define PLANGENERATION_H


#include <stdarg.h>

#include "../elementTemplates/menu.h"
#include "../elements/mainConfig.h"
#include "../elements/matchBrowser.h"

#include "../calc.h"


extern Settings* mainSettings;
extern Config* mainConfig;
extern ConfigExecutor* mainConfigExecutor;

struct PlanGenerator
{
	Template temp;

	Config* config;
	MatchBrowser* matchBrowser;
};
typedef struct PlanGenerator PlanGenerator;

struct PlanGeneratorExecutor
{
	Executor executor;

	ConfigExecutor* configExecutor;
	MatchBrowserExecutor* matchBrowserExecutor;
	
	int calculatedMatchesAmount;
};
typedef struct PlanGeneratorExecutor PlanGeneratorExecutor;


PlanGenerator* initPlanGenerator();
PlanGeneratorExecutor* initPlanGeneratorExecutor(PlanGenerator* planGenerator, WINDOW* win);


FlagCarrier planGeneratorInputHandler(PlanGeneratorExecutor* executor);

void printPlanGenerator(PlanGeneratorExecutor* planGenerator);


#endif

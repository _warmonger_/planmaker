#ifndef MATCHBROWSER_H
#define MATCHBROWSER_H

#define LESSON_EDITOR_SUBELEMENTS 3


#include <stdio.h>

#include "../structs.h"
#include "../flag.h"
#include "../elementTemplates/menu.h"
#include "../elementTemplates/config.h"
#include "../elementTemplates/chart.h"
#include "../elementTemplates/planChart.h"
#include "../elements/help.h"
#include "../elements/mainConfig.h"
#include "../elementTemplates/checkBox.h"


extern Settings* mainSettings;


struct MatchBrowser;
typedef struct MatchBrowser MatchBrowser;

struct MatchBrowserExecutor;
typedef struct MatchBrowserExecutor MatchBrowserExecutor;

struct MatchBrowser
{
	Template temp;
	
	PlanChart* planChart;
};

struct MatchBrowserExecutor
{
	Executor executor;
	
	ChartExecutor* planChartExecutor;
	
	int matchesLength;
	Match* matches;
	
	int currentMatch;
};

void initMatchBrowserAt(MatchBrowser* res);
MatchBrowser* initMatchBrowser();

void initMatchBrowserExecutorAt(MatchBrowserExecutor* res, MatchBrowser* matchBrowser, WINDOW* win, int matchesLength, Match* matches);
MatchBrowserExecutor* initMatchBrowserExecutor(MatchBrowser* matchBrowser,  WINDOW* win, int matchesLength, Match* matches);

/*Executor* getSelectedElement(MatchBrowserExecutor* executor);

int getSelectedMatchIndex(MatchBrowserExecutor* executor);
void setSelectedMatchIndex(MatchBrowserExecutor* executor, int selIndex);*/

void printMatchBrowser(MatchBrowserExecutor* executor);
FlagCarrier matchBrowserInputHandler(MatchBrowserExecutor* executor);

#endif

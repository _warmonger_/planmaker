#include "../elements/matchBrowser.h"

void convertPeriodToMatchChart(ChartExecutor* chartExecutor, Period* p, char* lessonName, char* vCode)
{
	int i, j;
	Chart* chart = (Chart*)getTemplate(chartExecutor);

	int pLen = mainSettings->periodLength;
	int psAmount = mainSettings->periodsAmount;
	Time* startTime = &mainSettings->startTime;


	int startCol = p->start.dayNumber;
	int endCol = p->end.dayNumber;
	int startRow;
	int endRow;

	char* preparedStr;

	startRow = timeToInt(&p->start);
	startRow = (startRow - (startRow % 60) - timeToInt(&mainSettings->startTime)) / pLen;

	endRow = timeToInt(&p->end);
	endRow = (endRow - (endRow % 60) - timeToInt(&mainSettings->startTime)) / pLen;
	
	i = startCol; j = startRow;
	
	preparedStr = malloc(strlen(lessonName) + 1 + strlen(vCode) + 1);
	memcpy(preparedStr, lessonName, strlen(lessonName));
	preparedStr[strlen(lessonName)] = ' ';
	memcpy(&preparedStr[strlen(lessonName) + 1], vCode, strlen(vCode));
	
	for(; i <= endCol; i++)
	{
		for(; (i < endCol && j < chart->maxRow) || (i == endCol && j <= endRow); j++)
		{
			chart->data[i][j] = preparedStr;
		}

		j = 0;
	}


}

void initMatchBrowserAt(MatchBrowser* res)
{
	initTemplateAt(&res->temp, false, (PrintFunc)printMatchBrowser, emptyUpdateFunc, (InputHandler)matchBrowserInputHandler, NULL);
	
	res->planChart = initDefaultPlanChart(NULL, 8);
}
MatchBrowser* initMatchBrowser()
{
	MatchBrowser* res = (MatchBrowser*)malloc(sizeof(MatchBrowser));

	initMatchBrowserAt(res);

	return res;
}

void initMatchBrowserExecutorAt(MatchBrowserExecutor* res, MatchBrowser* matchBrowser, WINDOW* win, int matchesLength, Match* matches)
{
	initExecutorAt(&res->executor, &matchBrowser->temp, win);
		
	initChartExecutorAt(res->planChartExecutor, (Chart*)matchBrowser->planChart, planChartJumpDeterminant, planChartHighLightDeterminant, win);

	res->matchesLength = matchesLength;
	res->matches = matches;
	
	res->currentMatch = 0;
	
	setSuperExecutor(res->planChartExecutor, res);
}
MatchBrowserExecutor* initMatchBrowserExecutor(MatchBrowser* matchBrowser,  WINDOW* win, int matchesLength, Match* matches)
{
	MatchBrowserExecutor* res = (MatchBrowserExecutor*)malloc(sizeof(MatchBrowser));
}

void printMatchBrowser(MatchBrowserExecutor* executor)
{
	int i, j;
	
	Chart* chart = (Chart*)getTemplate(executor->planChartExecutor);
	char*** data = chart->data;
	
	Match* match = &executor->matches[executor->currentMatch];
	
	
	for(i = 0; i < match->vDsLength; i++)
	{
		Variant* variant = &match->variantDatas[i].lesson->variants[match->variantDatas[i].variantIndex];
		
		for(j = 0; j < variant->periodsLength; j++)
		{
			convertPeriodToMatchChart(executor->planChartExecutor, &variant->periods[j], match->variantDatas[i].lesson->lessonName, variant->variantCode);
		}
	}
	
	callPrintFunc(executor->planChartExecutor);
	
	wprintw(getWindow(executor), "ESC by wyjsc");
}

FlagCarrier matchBrowserInputHandler(MatchBrowserExecutor* executor)
{
	FlagCarrier res =  initFlagCarrier();
	MatchBrowser* browser = (MatchBrowser*)getTemplate(executor);
	int readChar = getLastReadChar(executor);
	
	setFlag(&res,  IH_OK);
	
	if(readChar == KEY_F0)
	{
		setFlag(&res,  IH_STOP_EXECUTION);
	}
	else if(readChar == KEY_LEFT)
	{
		if(0 < executor->currentMatch)
		{
			executor->currentMatch--;
			
			setFlag(&res,  IH_CALL_UPDATE);
		}
	}
	else if(readChar == KEY_RIGHT)
	{
		if(executor->currentMatch + 1 < executor->matchesLength)
		{
			executor->currentMatch++;
			
			setFlag(&res,  IH_CALL_UPDATE);
		}
	}
	else
	{
		setFlag(&res,  IH_IGNORED_CHAR);
	}
	
	return res;
}

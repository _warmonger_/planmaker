#ifndef HELP_H
#define HELP_H

#define LINES_PER_SITE 10
#define CHARS_PER_LINE 70

#include <ncurses.h>

#include "../elementTemplates/menu.h"
#include "../elementTemplates/textBox.h"


struct Help
{
	Template temp;
	
	Menu* helpMenu;

	int textBoxesLength;
	TextBox* textBoxes;
};
typedef struct Help Help;

struct HelpExecutor
{
	Executor executor;
	
	MenuExecutor* helpMenuExecutor;
	TextBoxExecutor* textBoxExecutors;
};
typedef struct HelpExecutor HelpExecutor;


Help* prepareDefaultHelp();


void initHelpAt(Help* res, int length, char** titles, char** texts);
Help* initHelp(int length, char** titles, char** texts);

void initHelpExecutorAt(HelpExecutor* res, Help* temp, WINDOW* win);
HelpExecutor* initHelpExecutor(Help* temp, WINDOW* win);

void printHelp(HelpExecutor* executor);

FlagCarrier helpInputHandler(HelpExecutor* executor);
FlagCarrier helpMenuHandler(MenuExecutor* menuExecutor);

#endif

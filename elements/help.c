#include "../elements/help.h"

void initHelpAt(Help* res, int length, char** titles, char** texts)
{
	int i;
	Menu* helpMenu;

	initTemplateAt(&res->temp, false, (PrintFunc)printHelp, emptyUpdateFunc, (InputHandler)helpInputHandler, NULL);

	helpMenu = initMenu((ClickHandler)helpMenuHandler, length + 1);

	helpMenu->menuDesc = "Pomoc";
	

	for(i = 0; i < length; i++)
	{
		helpMenu->menuOptions[i] = titles[i];
	}

	helpMenu->menuOptions[length] = "Idz do Menu";

	res->helpMenu = helpMenu;
	
	res->textBoxesLength = length;	
	res->textBoxes = (TextBox*)malloc(sizeof(TextBox) * length);
	
	for(i = 0; i < length; i++)
	{
		initTextBoxAt(&res->textBoxes[i], NULL, texts[i]);
	}
}

Help* initHelp(int length, char** titles, char** texts)
{
	Help* res = (Help*)malloc(sizeof(Help));
	
	initHelpAt(res, length, titles, texts);
	
	return res;
}

void initHelpExecutorAt(HelpExecutor* res, Help* temp, WINDOW* win)
{
	int i;

	initExecutorAt(&res->executor, &temp->temp, win);

	res->helpMenuExecutor = initMenuExecutor(temp->helpMenu, win);

	res->textBoxExecutors = (TextBoxExecutor*)malloc(sizeof(TextBoxExecutor) * temp->textBoxesLength);
	
	for(i = 0; i < temp->textBoxesLength; i++)
	{
		initTextBoxExecutorAt(&res->textBoxExecutors[i], &temp->textBoxes[i], win);

		setSuperExecutor(&res->textBoxExecutors[i], res->helpMenuExecutor);
	}

	setSuperExecutor(res->helpMenuExecutor, res);
}

HelpExecutor* initHelpExecutor(Help* temp, WINDOW* win)
{
	HelpExecutor* res = (HelpExecutor*)malloc(sizeof(HelpExecutor));
	
	initHelpExecutorAt(res, temp, win);
	

	return res;
}

Help* prepareDefaultHelp()
{
	int i, len;
	int boxLength = 0, buffLength = 0, fileCounter = 0;
	DIR *dp;
	struct dirent *ep;
	FILE* file;
	char** titles;
	char** texts;
	char* nameBuff = (char*)malloc(sizeof(char) * 256);

    	dp = opendir ("./help");
	
	if (dp != NULL)
	{
		while (ep = readdir (dp))
		{
			if (!strcmp (ep->d_name, ".") || !strcmp (ep->d_name, ".."))
			{
				continue;
			}
			
			snprintf(nameBuff, 256, "./help/%s", ep->d_name);
			file = fopen(nameBuff, "r");
			
			if (file == NULL)
			{
				fprintf(stderr, "1Error : Failed to open entry file - %s\n", nameBuff);

				return;
			}
			fclose(file);
			
			boxLength++;
		}
							
		(void) closedir (dp);
	}
	else
	{
		perror ("Couldn't open the directory");
		
		return;
	}
	
	titles = (char**)malloc(sizeof(char*) * boxLength);
	texts =  (char**)malloc(sizeof(char*) * boxLength);

	dp = opendir ("./help");
	if (dp != NULL)
	{		
		for (i = 0; (ep = readdir (dp)) && i < boxLength; i++)
		{
			if (!strcmp (ep->d_name, ".") || !strcmp (ep->d_name, ".."))
			{
				i--;

				continue;
			}
				   
			snprintf(nameBuff, 256, "./help/%s", ep->d_name);
			file = fopen(nameBuff, "r");
			
			if (file == NULL)
			{
				fprintf(stderr, "2Error : Failed to open entry file - %s\n", nameBuff);

				return;
			}
			fseek(file, 0L, SEEK_END);
			len = ftell(file);

			rewind(file);

			{
				texts[i] = (char*)malloc(sizeof(char) * (len + 1));
				
				fread(texts[i], len, 1, file);
				texts[i][len] = 0;
				
				titles[i] = (char*)malloc(sizeof(char) * (strlen(ep->d_name) + 1));
				strcpy(titles[i], ep->d_name);
			}			
			
			fclose(file);
		}

		(void) closedir (dp);
	}
	else
	{
		perror ("Couldn't open the directory");
	}

	return initHelp(boxLength, titles, texts);
}

FlagCarrier helpInputHandler(HelpExecutor* executor)
{
	return callInputHandler(executor->helpMenuExecutor, getLastReadChar(executor));
}

FlagCarrier helpMenuHandler(MenuExecutor* menuExecutor)
{
	FlagCarrier res =  initFlagCarrier();
	Menu* menu = (Menu*)getTemplate(menuExecutor);
	WINDOW* win = getWindow(menuExecutor);

	int selectedOption = getSelectedOption(menuExecutor);
	Help* h = (Help*)getTemplate(getSuperExecutor(menuExecutor));
	HelpExecutor* hE = (HelpExecutor*)getSuperExecutor(menuExecutor);

	
	setFlag(&res,  IH_OK);

	if(selectedOption < menu->menuOptionsLength - 1)
	{
		execute(&hE->textBoxExecutors[selectedOption]);

		setFlag(&res,  IH_CALL_UPDATE);
	}
	else
	{
		setFlag(&res,  IH_STOP_EXECUTION);
	}

	return res;
}

void printHelp(HelpExecutor* executor)
{
	callPrintFunc(executor->helpMenuExecutor);
	/*callPrintFunc(&executor->textBoxes[executor->currentTextBox]);*/
}

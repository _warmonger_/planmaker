#include "../elements/lessonEditor.h"

char compareVariants(char* v0, char* v1)
{
	return (v0 == NULL || v1 == NULL) ? (v0 == v1) : !strcmp(v0, v1);
}

void convertPeriodToChart(ChartExecutor* chartExecutor, Period* p, char* vCode)
{
	int i, j;
	Chart* chart = (Chart*)getTemplate(chartExecutor);

	int pLen = mainSettings->periodLength;
	int psAmount = mainSettings->periodsAmount;
	Time* startTime = &mainSettings->startTime;


	int startCol = p->start.dayNumber;
	int endCol = p->end.dayNumber;
	int startRow;
	int endRow;

	startRow = timeToInt(&p->start);
	startRow = (startRow - (startRow % 60) - timeToInt(&mainSettings->startTime)) / pLen;

	endRow = timeToInt(&p->end);
	endRow = (endRow - (endRow % 60) - timeToInt(&mainSettings->startTime)) / pLen;
	
	i = startCol; j = startRow;
	for(; i <= endCol; i++)
	{
		for(; (i < endCol && j < chart->maxRow) || (i == endCol && j <= endRow); j++)
		{
			chart->data[i + 1][j + 1] = vCode;
		}
	}


}

void loadLesson(LessonEditorExecutor* editor, Lesson* l)
{
	int i, j;
	Chart* chart = (Chart*)getTemplate(editor->planChartExecutor);
	
	for(i = 0; i < l->variantsLength; i++)
	{
		for(j = 0; j < l->variants[i].periodsLength; j++)
		{
			convertPeriodToChart(editor->planChartExecutor, &l->variants[i].periods[j], l->variants[i].variantCode);
		}
	}

	editor->currLesson = l;
}

void clearLessonEditor(LessonEditorExecutor* editor)
{
	int i, j;

	editor->currLesson = NULL;

	Chart* chart = (Chart*)getTemplate(editor->planChartExecutor);

	for(i = 1; i < chart->maxCol; i++)
	{
		for(j = 1; j < chart->maxRow; j++)
		{
			chart->data[i][j] = NULL;
		}
	}
}	

void extractLessonInto(Lesson* res, LessonEditorExecutor* editor)
{
	int i, j, k;
	char* lastVariant = NULL;
	int variantsLength = 0;
	Variant* variants = (Variant*)malloc(sizeof(Variant) * 256);

	Chart* chart = (Chart*)getTemplate(editor->planChartExecutor);
	char*** data = getChartData(editor->planChartExecutor);
	
	int maxCol = ((Chart*)getTemplate(editor->planChartExecutor))->maxCol;
	int maxRow = ((Chart*)getTemplate(editor->planChartExecutor))->maxRow;
	
	Time* startTime = initTime();
	Time* currTime = initTime();
	Time* periodTime = intToTime(mainSettings->periodLength);

	int periodLength;

	for(i = 0; i < 256; i++)
	{
		variants[i].variantCode = 0;

		variants[i].periodsLength = 0;
		variants[i].periods = (Period*)malloc(sizeof(Period) * 256);
	}
	
	for(i = 1; i < maxCol; i++)
	{	
		startTime->dayNumber = i;
		startTime->hour = mainSettings->startTime.hour;	
		startTime->minute = mainSettings->startTime.minute;

		memcpy(currTime, startTime, sizeof(Time));


		for(j = 1; j < maxRow; j++)
		{
			increaseTimeBy(currTime, periodTime);
			
			if(!compareVariants(lastVariant, data[i][j]))
			{				
				for(k = 0; k < variantsLength; k++)
				{
					if(compareVariants(variants[k].variantCode, data[i][j]))
					{
						Period* p = &variants[k].periods[variants[k].periodsLength];

						memcpy(&p->start, startTime, sizeof(Time));
						memcpy(&p->end, currTime, sizeof(Time));

						variants[k].periodsLength++;

						break;
					}
				}

				if(k >= variantsLength)
				{
					Period* p;
					
					if(data[i][j] == NULL)
					{
						variants[variantsLength].variantCode = "";
					}
					else
					{
						variants[variantsLength].variantCode = (char*)malloc(strlen(data[i][j]) + 1);
						memcpy(variants[variantsLength].variantCode, data[i][j], strlen(data[i][j]) + 1);
					}

					p = &variants[variantsLength].periods[variants[variantsLength].periodsLength];

					memcpy(&p->start, startTime, sizeof(Time));
					memcpy(&p->end, currTime, sizeof(Time));

					variants[variantsLength].periodsLength++;
					variantsLength++;
				}
				
				memcpy(startTime, currTime, sizeof(Time));
			}

			lastVariant = data[i][j];
		}
		
		lastVariant = NULL;
	}

	res->lessonName = getText(editor->lessonNameInputBoxExecutor);
	res->variantsLength = variantsLength;
	res->variants = (Variant*)malloc(sizeof(Variant) * variantsLength);

	for(i = 0; i < res->variantsLength; i++)
	{
		res->variants[i].variantCode = variants[i].variantCode;
		res->variants[i].periodsLength = variants[i].periodsLength;

		res->variants[i].periods = (Period*)malloc(sizeof(Period) * res->variants[i].periodsLength);

		for(j = 0; j < res->variants[i].periodsLength; j++)
		{
			memcpy(&res->variants[i].periods[j], &variants[i].periods[j], sizeof(Period));
		}

		for(j = 0; j < variants[i].periodsLength; j++)
		{
			free(variants[i].periods);
		}
	}

	free(variants);
}

Lesson* extractLesson(LessonEditorExecutor* editor)
{
	Lesson* res = (Lesson*)malloc(sizeof(Lesson));
	
	extractLessonInto(res, editor);

	return res;	
}

void initLessonEditorAt(LessonEditor* res)
{
	Time* endTime;

	initTemplateAt(&res->temp, false, (PrintFunc)printLessonEditor, emptyUpdateFunc, (InputHandler)lessonEditorInputHandler, NULL);	
	
	endTime = timeByTime(mainSettings->startTime.dayNumber, mainSettings->startTime.hour, mainSettings->startTime.minute);
	increaseTimeBy(endTime, timeByTime(0, 0, mainSettings->periodLength * mainSettings->periodLength));
	
	res->planChart = initDefaultPlanChart((ClickHandler)lEChartClickHandler, 8);

	res->varCodeInputBox = initInputBox("Nazwa wariantu");
	res->lessonNameInputBox = initInputBox("Nazwa lekcji");
	res->isRequiredCheckBox = initDefaultCheckBox("Czy wymagany?");
}

LessonEditor* initLessonEditor()
{
	LessonEditor* res = (LessonEditor*)malloc(sizeof(LessonEditor));
	
	initLessonEditorAt(res);
	
	return res;
}

void initLessonEditorExecutorAt(LessonEditorExecutor* res, LessonEditor* lessonEditor, WINDOW* win)
{
	WINDOW* chartWindow, *varWindow, *lessonWindow, *requiredWindow;
	initExecutorAt(&res->executor, &lessonEditor->temp, win);
	
	chartWindow = derwin(win, getMaxWinY(res) - LESSON_EDITOR_SUBELEMENTS - 1, getMaxWinX(res), 0, 0);
	varWindow = derwin(win, 1, getMaxWinX(res), getMaxWinY(res) - LESSON_EDITOR_SUBELEMENTS - 1, 0);
	lessonWindow = derwin(win, 1, getMaxWinX(res), getMaxWinY(res) - LESSON_EDITOR_SUBELEMENTS, 0);
	requiredWindow = derwin(win, 1, getMaxWinX(res), getMaxWinY(res) - LESSON_EDITOR_SUBELEMENTS + 1, 0);
	
	res->currentState = 0;
	
	res->planChartExecutor = initPlanChartExecutor(lessonEditor->planChart, chartWindow);
	res->varCodeInputBoxExecutor = initInputBoxExecutor(lessonEditor->varCodeInputBox, varWindow);
	res->lessonNameInputBoxExecutor = initInputBoxExecutor(lessonEditor->lessonNameInputBox, lessonWindow);
	res->isRequiredCheckBoxExecutor = initCheckBoxExecutor(lessonEditor->isRequiredCheckBox, requiredWindow);
	
	setSuperExecutor(res->planChartExecutor, res);
	setSuperExecutor(res->varCodeInputBoxExecutor, res);
	setSuperExecutor(res->lessonNameInputBoxExecutor, res); 
	setSuperExecutor(res->isRequiredCheckBoxExecutor , res);
}

LessonEditorExecutor* initLessonEditorExecutor(LessonEditor* lessonEditor,  WINDOW* win)
{
	LessonEditorExecutor* res = (LessonEditorExecutor*)malloc(sizeof(LessonEditorExecutor));
	
	initLessonEditorExecutorAt(res, lessonEditor, win);
	
	return res;
}

Executor* getLessonEditorElement(LessonEditorExecutor* executor, int index)
{
	switch(index)
	{
		case 0:
			return &executor->planChartExecutor->executor;
		case 1:
			return &executor->varCodeInputBoxExecutor->executor;
		case 2:
			return &executor->lessonNameInputBoxExecutor->executor;
		case 3:
			return &executor->isRequiredCheckBoxExecutor->executor;
		default:
			return NULL;
	}
}

Executor* getSelectedLessonEditorElement(LessonEditorExecutor* executor)
{
	return getLessonEditorElement(executor, executor->currentState);
}

void printLessonEditor(LessonEditorExecutor* executor)
{
	int i;

	WINDOW* win = getWindow(executor);
	Executor* ex;

	wclear(getWindow(executor));
	
	for(i = 0; i < 4; i++)
	{
		if(i == executor->currentState) { continue; }
		
		ex = getLessonEditorElement(executor, i);

		printElement(ex);
		wrefresh(getWindow(ex));
	}
	
	ex = getSelectedLessonEditorElement(executor);	
	
	printElement(ex);

	wrefresh(getWindow(executor));

	wrefresh(getWindow(ex));
}

FlagCarrier lessonEditorInputHandler(LessonEditorExecutor* executor)
{
	FlagCarrier res = *initFlagCarrier();
	LessonEditor* lessonEditor = (LessonEditor*)getTemplate(executor);
	int readChar = getLastReadChar(executor);
	
	
	setFlag(&res,  IH_OK);
	
	if(readChar == KEY_NPAGE)
	{
		if(executor->currentState + 1 < LESSON_EDITOR_SUBELEMENTS)
		{
			executor->currentState++;

			setFlag(&res,  IH_CALL_UPDATE);

			curs_set(executor->currentState == 0 ? 0 : 1);

			callUpdateFunc(getSelectedLessonEditorElement(executor));
		}
	}
	else if (readChar == KEY_PPAGE)
	{
		if(0 < executor->currentState)
		{
			executor->currentState--;

			setFlag(&res,  IH_CALL_UPDATE);

			curs_set(executor->currentState == 0 ? 0 : 1);

			callUpdateFunc(getSelectedLessonEditorElement(executor));
		}
	}
	else if(readChar == CTRL('s'))
	{
		extractLessonInto(executor->currLesson, executor);
		
		setFlag(&res,  IH_STOP_EXECUTION);
	}
	else
	{
		Executor* subExecutor = getSelectedLessonEditorElement(executor);
		
		FlagCarrier subRes = callInputHandler(subExecutor, readChar);
		
		if(getFlag(subRes, IH_CALL_UPDATE))
		{
			callUpdateFunc(subExecutor);
		}

		setFlag(&res,  getFlag(subRes, IH_OK));
		setFlag(&res,  getFlag(subRes, IH_IGNORED_CHAR));
	}
	
	return res;
}

FlagCarrier lEChartClickHandler(ChartExecutor* executor)
{
	FlagCarrier res = *initFlagCarrier();
	int readChar = getLastReadChar(executor);

	LessonEditorExecutor* superEx = (LessonEditorExecutor*)getSuperExecutor(executor);
	PlanChart* planChart = (PlanChart*)getTemplate(executor);	

	if(readChar == KEY_ENTER || readChar == '\n' || readChar == ' ')
	{
		int selCol = executor->selectedCol;
		int selRow = executor->selectedRow;
		
		char** line = &planChart->rawChart.data[selCol][selRow];

		if(*line == NULL || strlen(*line) == 0 || strcmp(*line, superEx->varCodeInputBoxExecutor->value))
		{
			*line = (char*)malloc(strlen(superEx->varCodeInputBoxExecutor->value) + 1);

			memcpy(*line, superEx->varCodeInputBoxExecutor->value, strlen(superEx->varCodeInputBoxExecutor->value) + 1);

		}
		else
		{
			free(*line);
	
			*line = NULL;
		}

		setFlag(&res, IH_CALL_UPDATE);
	}

	return res;
}

#include "../elements/mainMenu.h"

SettingsIO* settingsIO = NULL;
SettingsIOExecutor* settingsIOExecutor = NULL;

Config* mainConfig = NULL;
ConfigExecutor* mainConfigExecutor = NULL;

LessonManager* lessonManager = NULL;
LessonManagerExecutor* lessonManagerExecutor = NULL;

PlanGenerator* planGenerator = NULL;
PlanGeneratorExecutor* planGeneratorExecutor = NULL;

Help* help = NULL;
HelpExecutor* helpExecutor = NULL;

void _initMainMenu()
{
	settingsIO = initSettingsIO(INPUT_MODE);
	settingsIOExecutor = initSettingsIOExecutor(settingsIO, stdscr);
	
	mainConfig = prepareMainConfig();
	mainConfigExecutor = prepareMainConfigExecutor(mainConfig, stdscr);
	
	lessonManager = initLessonManager();
	lessonManagerExecutor = initLessonManagerExecutor(lessonManager, stdscr);
	
	planGenerator = initPlanGenerator();
	planGeneratorExecutor = initPlanGeneratorExecutor(planGenerator, stdscr);
	
	help = prepareDefaultHelp();
	if(help != NULL) { helpExecutor = initHelpExecutor(help, stdscr); }
}

FlagCarrier standardHandler(MenuExecutor* menuExecutor)
{
	FlagCarrier res =  initFlagCarrier();
	setFlag(&res,  IH_OK);
	
	switch(getSelectedOption(menuExecutor))
	{
		case 0:
			settingsIO->mode = INPUT_MODE;
			execute(settingsIOExecutor);
			break;
		case 1:
			execute(lessonManagerExecutor);
			break;
		case 2:
			resetConfigValues(planGeneratorExecutor->configExecutor);

			execute(planGeneratorExecutor);
			break;
		case 3:
			settingsIO->mode = OUTPUT_MODE;
			execute(settingsIOExecutor);
			break;
		case 4:
			if(help == NULL)
			{
				wprintw(getWindow(menuExecutor), "Pomoc nie zostala zaladowana!");

				break;
			}
			
			execute(helpExecutor);
		
			break;
		case 5:
			endwin();

			exit(-1);
		default:
			setFlag(&res,  IH_IGNORED_CHAR);
			return res;
	}
	
	setFlag(&res,  IH_CALL_UPDATE);

	
	return res;
}

Menu* prepareMainMenu()
{
	Menu* res = initMenu((ClickHandler)standardHandler, 6);

	res->menuDesc = "Program do ukladania planu lekcji. Wybierz jedna z ponizszych opcji:\n\n";

	res->menuOptions[0] = "Wczytaj ustawienia";
	res->menuOptions[1] = "Edytuj harmonogramy";
	res->menuOptions[2] = "Generuj plany lekcji";
	res->menuOptions[3] = "Zapisz ustawienia";
	res->menuOptions[4] = "Pomoc";
	res->menuOptions[5] = "Wyjdz";

	return res;
}

MenuExecutor* prepareMainMenuExecutor(Menu* m, WINDOW* win)
{
	MenuExecutor* res = initMenuExecutor(m, win);

	return res;
}

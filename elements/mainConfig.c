#include "../elements/mainConfig.h"

void resetConfigValues(ConfigExecutor* configExecutor)
{
	Config* config = (Config*)getTemplate(configExecutor);
	
	char* buffer = (char*)malloc(getMaxWinX(configExecutor)), *buffer2;

	sprintf(buffer, "%d", mainSettings->maxCollisionLength);
	memcpy(configExecutor->fieldExecutors[0].value, buffer, strlen(buffer) + 1);
	configExecutor->fieldExecutors[0].lastIndex = strlen(buffer);

	sprintf(buffer, "%d", mainSettings->maxCollisionAmount);
	memcpy(configExecutor->fieldExecutors[1].value, buffer, strlen(buffer) + 1);
	configExecutor->fieldExecutors[1].lastIndex = strlen(buffer);

	sprintf(buffer, "%d", mainSettings->maxGeneratedPlanAmount);
	memcpy(configExecutor->fieldExecutors[2].value, buffer, strlen(buffer) + 1);
	configExecutor->fieldExecutors[2].lastIndex = strlen(buffer);

	buffer2 = timeToString(&mainSettings->startTime);
	memcpy(configExecutor->fieldExecutors[3].value, buffer2, strlen(buffer2) + 1);
	configExecutor->fieldExecutors[3].lastIndex = strlen(buffer);

	sprintf(buffer, "%d", mainSettings->periodLength);
	memcpy(configExecutor->fieldExecutors[4].value, buffer, strlen(buffer) + 1);
	configExecutor->fieldExecutors[4].lastIndex = strlen(buffer);

	sprintf(buffer, "%d", mainSettings->periodsAmount);
	memcpy(configExecutor->fieldExecutors[5].value, buffer, strlen(buffer) + 1);
	configExecutor->fieldExecutors[5].lastIndex = strlen(buffer);
}

char isDigit(int character)
{
	return character > 47 && character < 58;
}

char timeHandler(InputBox* inputBox, int character)
{
	return isDigit(character) || character == ':';
}

char saveConfig(ConfigExecutor* configExecutor)
{
	int res;

	res = sscanf(configExecutor->fieldExecutors[0].value, "%d", &mainSettings->maxCollisionLength);
	if(res < 1) { return 0; }

	res = sscanf(configExecutor->fieldExecutors[1].value, "%d", &mainSettings->maxCollisionAmount);
	if(res < 1) { return 0; }

	res = sscanf(configExecutor->fieldExecutors[2].value, "%d", &mainSettings->maxGeneratedPlanAmount);
	if(res < 1) { return 0; }

	res = sscanf(configExecutor->fieldExecutors[3].value, "%d:%d", &mainSettings->startTime.hour, &mainSettings->startTime.minute);
	if(res < 2) { return 0; }

	res = sscanf(configExecutor->fieldExecutors[4].value, "%d", &mainSettings->periodLength);
	if(res < 1) { return 0; }

	res = sscanf(configExecutor->fieldExecutors[5].value, "%d", &mainSettings->periodsAmount);
	if(res < 1) { return 0; }

	return 1;
}

Config* prepareMainConfig()
{
	Config* res = initConfig("Ustawienia", 6, saveConfig);
	
	setFieldDesc(res, 0, "Dopuszczalna dlugosc kolizji zajec");
	setFieldDesc(res, 1, "Dopuszczalna ilosc kolizji");
	setFieldDesc(res, 2, "Maksymalna liczba wygenerowanych planow");
	setFieldDesc(res, 3, "Godzina poczatku zajec (HH:MM)");
	setFieldDesc(res, 4, "Okres przedzialu (minuty)");
	setFieldDesc(res, 5, "Ilosc przedzialow");

	return res;
}

ConfigExecutor* prepareMainConfigExecutor(Config* config, WINDOW* win)
{
	int i;
	ConfigExecutor* res = initConfigExecutor(config, win);

	for(i = 0; i < config->fieldsLength; i++)
	{
		config->fields[i].temp.clickHandler = (ClickHandler)numberHandler;
		res->fieldExecutors[i].maxValuePostDefinedLength = res->fieldExecutors[i].executor.maxWinX - strlen(config->fields[i].desc) - 1;
	}

	config->fields[3].temp.clickHandler = (ClickHandler)numberHandler;

	resetConfigValues(res);

	return res;

}

#include "planGeneration.h"

PlanGenerator* initPlanGenerator()
{
	PlanGenerator* res = (PlanGenerator*)malloc(sizeof(PlanGenerator));
	
	initTemplateAt(&res->temp, true, (PrintFunc)printPlanGenerator, emptyUpdateFunc, (InputHandler)planGeneratorInputHandler, (ClickHandler)NULL);
	
	res->config = prepareMainConfig();
	res->matchBrowser = initMatchBrowser();
}

PlanGeneratorExecutor* initPlanGeneratorExecutor(PlanGenerator* planGenerator, WINDOW* win)
{
	WINDOW* configWin;
	
	PlanGeneratorExecutor* res = (PlanGeneratorExecutor*)malloc(sizeof(PlanGenerator));	
	initExecutorAt(&res->executor, &planGenerator->temp, win);
	
	configWin = derwin(win, getMaxWinY(res) / 2, getMaxWinX(res), 0, 0);	

	keypad(configWin, TRUE);

	res->configExecutor = prepareMainConfigExecutor(planGenerator->config, configWin);
	res->matchBrowserExecutor = initMatchBrowserExecutor(planGenerator->matchBrowser, win, 0, NULL);

	res->calculatedMatchesAmount = 10;
	
	setSuperExecutor(res->configExecutor, res);
	setSuperExecutor(res->matchBrowserExecutor, res);
	
	return res;
}

void refreshDescription(PlanGeneratorExecutor* executor)
{
	WINDOW* win = getWindow(executor);

	wmove(win, getMaxWinY(executor->configExecutor) + 2, 0);
	wclrtoeol(win);
	wprintw(win, "Dopasowania poprawne dla konfiguracji: ");

	if(mainSettings->maxGeneratedPlanAmount < 0 || executor->calculatedMatchesAmount <= mainSettings->maxGeneratedPlanAmount)
	{
		wprintw(win, "%d\n", executor->calculatedMatchesAmount);
	}
	else
	{
		wprintw(win, "%d (bez limitu: %d)\n", mainSettings->maxGeneratedPlanAmount, executor->calculatedMatchesAmount);
	}

	wprintw(win, "CTRL+R - wroc bez zapisywania;\nCTRL+E - zapisz i przelicz ponownie\nCTRL+S - zapisz");
}

void printPlanGenerator(PlanGeneratorExecutor* executor)
{
	WINDOW* win = getWindow(executor);
	
	
	wclear(win);
	
	refreshDescription(executor);

	printElement(executor->configExecutor);
	
	wrefresh(win);

	resetCursor(executor->configExecutor);
}

FlagCarrier planGeneratorInputHandler(PlanGeneratorExecutor* executor)
{
	FlagCarrier res =  initFlagCarrier();
	PlanGenerator* planGenerator = (PlanGenerator*)getTemplate(executor);
	WINDOW* win = getWindow(executor);
	int readChar = getLastReadChar(executor);
	
	
	setFlag(&res,  IH_OK);
	
	if(readChar == CTRL('r'))
	{
		setFlag(&res,  IH_STOP_EXECUTION);

		resetConfigValues(res);
	}
	if(readChar == CTRL('e'))
	{
		int cMI = 0;
		Lesson** lss = convertLessons();

		executor->calculatedMatchesAmount = 0;


		if(mainSettings->lessonsLength == 0) { setFlag(&res,  IH_CALL_UPDATE); return res; }

		computeMatches(lss, getListLength(mainSettings->lessons), 0, executor->matchBrowserExecutor->matches, 256);

		setFlag(&res,  IH_CALL_UPDATE);
	}
	else if(readChar == CTRL('s'))
	{
		if(((Config*)getTemplate(executor->configExecutor))->saveConfig(executor->configExecutor))
		{
			setFlag(&res,  IH_STOP_EXECUTION);
		}
		else
		{
			WINDOW* win = getWindow(executor);

			setFlag(&res,  IH_STOP_EXECUTION);

			wprintw(win, "Nie mozna zapisac konfiguracji, uruchom program ponownie?");

			wrefresh(win);
		}
		
	}
	else if(readChar == ' ')
	{
		Match* buffMatch = initMatch(256);
		Lesson** lss = convertLessons();
		computeMatchesAmount(lss, getListLength(mainSettings->lessons), 0, buffMatch, &executor->calculatedMatchesAmount);
		free(buffMatch);
	}
	else
	{
		FlagCarrier subRes = callInputHandler(executor->configExecutor, readChar);

		if(getFlag(subRes, IH_CALL_UPDATE))
		{
			setFlag(&res,  IH_CALL_UPDATE);
		}
		
		if(getFlag(subRes, IH_IGNORED_CHAR))
		{
			setFlag(&res,  IH_IGNORED_CHAR);
		}
	}

	resetCursor(executor->configExecutor);
	
	return res;
}

int configEnterHandler(ConfigExecutor* executor)
{
	PlanGeneratorExecutor* pGExecutor = (PlanGeneratorExecutor*)getSuperExecutor(executor);
	
	Match* buffMatch = initMatch(256);
	computeMatchesAmount(mainSettings->lessons, getListLength(mainSettings->lessons), 0, buffMatch, &pGExecutor->calculatedMatchesAmount);
	free(buffMatch);
	
	printElement(pGExecutor);
	
	return IH_OK;
}

#ifndef MAINCONFIG_H
#define MAINCONFIG_H

#include "../elementTemplates/config.h"

#include "../structs.h"

extern Settings* mainSettings;

void resetConfigValues(ConfigExecutor* configExecutor);

Config* prepareMainConfig();
ConfigExecutor* prepareMainConfigExecutor(Config* c, WINDOW* win);


#endif

#include "../elements/settingsIO.h"

char* titles[4] = { "Wczytaj ustawienia", "Zapisz ustawienia", "Podaj sciezke do pliku ustawien", "Zapisz jako"};

const char* badFile = "Nie mozna otworzyc podanego pliku!";

char inputSettings(const char* filename)
{
	FILE* file = fopen(filename, "r");
	
	if(file == NULL)
	{
		return 0;
	}
	
	mainSettings = readSettings(file);
	
	fclose(file);
	
	return 1;
}

char outputSettings(const char* filename)
{
	FILE* file = fopen(filename, "w");
	
	if(file == NULL)
	{
		return 0;
	}
	
	writeSettings(file, mainSettings);
	
	fclose(file);
	
	return 1;
	
}

FlagCarrier settingsIOInputHandler(SettingsIOExecutor* settingsIOExecutor)
{
	FlagCarrier res, inputHandlerRes;
	WINDOW* win = getWindow(settingsIOExecutor);
	SettingsIO* settingsIO = (SettingsIO*)getTemplate(settingsIOExecutor);
	
	int readChar = getLastReadChar(settingsIOExecutor);
	
	
	setFlag(&res,  IH_OK);

	if(readChar == KEY_ENTER || readChar == '\n')
	{
		char result = settingsIO->mode ? outputSettings(settingsIOExecutor->pathInputBoxExecutor->value) : inputSettings(settingsIOExecutor->pathInputBoxExecutor->value);
		
		if(result)
		{
			setFlag(&res, IH_STOP_EXECUTION);
		}
		else
		{
			clearInputBox(settingsIOExecutor->pathInputBoxExecutor);
			settingsIOExecutor->printBadFile = 1;

			setFlag(&res, IH_CALL_UPDATE);
		}
	}
	
	inputHandlerRes	= callInputHandler(settingsIOExecutor->pathInputBoxExecutor, readChar);

	if(getFlag(inputHandlerRes, IH_IGNORED_CHAR))
	{
		setFlag(&res,  IH_IGNORED_CHAR);
	}
	
	return res;
}

void initSettingsIOAt(SettingsIO* res, char mode)
{
	initTemplateAt(&res->temp, true, (PrintFunc)printSettingsIO, emptyUpdateFunc, (InputHandler)settingsIOInputHandler, NULL);

	res->pathInputBox = initInputBox(titles[mode + 2]);
	res->mode = mode;
}

SettingsIO* initSettingsIO(char mode)
{
	SettingsIO* res = (SettingsIO*)malloc(sizeof(SettingsIO));

	initSettingsIOAt(res, mode);

	return res;
}

void initSettingsIOExecutorAt(SettingsIOExecutor* res, SettingsIO* settingsIO, WINDOW* win)
{
	WINDOW* inputWin;
	initExecutorAt(&res->executor, &settingsIO->temp, win);
	
	inputWin = derwin(win, 1, getMaxWinX(res), 2, 0);
	res->pathInputBoxExecutor = initInputBoxExecutor(settingsIO->pathInputBox, inputWin);
	res->printBadFile = 0;

	setSuperExecutor(res->pathInputBoxExecutor, res);
}

SettingsIOExecutor* initSettingsIOExecutor(SettingsIO* settingsIO, WINDOW* win)
{
	SettingsIOExecutor* res = (SettingsIOExecutor*)malloc(sizeof(SettingsIOExecutor));
	
	initSettingsIOExecutorAt(res, settingsIO, win);

	return res;
}

void printSettingsIO(SettingsIOExecutor* executor)
{
	WINDOW* win = getWindow(executor);
	SettingsIO* settings = (SettingsIO*)getTemplate(executor);
	
	if(getCleanWindow(executor)) { wclear(win); }

	wprintw(win, "%s\n\n\n", titles[settings->mode]);
	
	callPrintFunc(executor->pathInputBoxExecutor);

	if(executor->printBadFile)
	{
		executor->printBadFile = 0;

		wprintw(win, "%s", badFile);
	}

	wrefresh(win);

	setCursorAt(executor->pathInputBoxExecutor, 0);
}

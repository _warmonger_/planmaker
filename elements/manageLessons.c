#include "../elements/manageLessons.h"

LessonEditor* lessonEditor;
LessonEditorExecutor* lessonEditorExecutor = NULL;

void _initLessonManager()
{
	lessonEditor = initLessonEditor();

	lessonEditorExecutor = initLessonEditorExecutor(lessonEditor, stdscr);
}

FlagCarrier listClickHandler(ListExecutor* executor)
{
	FlagCarrier res =  initFlagCarrier();
	setFlag(&res, IH_OK);
	
	lessonEditorExecutor->currLesson = (Lesson*)getElement(mainSettings->lessons, executor->selectedRow)->data;

	if(lessonEditorExecutor->currLesson != NULL)
	{
		setFlag(&res,  getFlag(execute(lessonEditorExecutor), IH_OK));
	}

	return res;
}

FlagCarrier chartClickHandler(ChartExecutor* executor)
{
	FlagCarrier res =  initFlagCarrier();
	LessonManagerExecutor* superEx = (LessonManagerExecutor*)getSuperExecutor(executor);
	ListExecutor* listExecutor = superEx->lessonListExecutor;
	
	Lesson* newLesson;
	PtrEntry* lesson;

	setFlag(&res, IH_OK);
	
	switch(executor->selectedCol)
	{
		case 0:
			newLesson = initLesson();
			loadLesson(lessonEditorExecutor, newLesson);
			execute(lessonEditorExecutor);

			free(newLesson);
			
			addLesson(extractLesson(lessonEditorExecutor));

			clearLessonEditor(lessonEditorExecutor);
			break;
		case 1:
			lesson = getElement(mainSettings->lessons, listExecutor->selectedRow);
		
			loadLesson(lessonEditorExecutor, (Lesson*)lesson->data);
			execute(lessonEditorExecutor);

			free(lesson->data);

			lesson->data = extractLesson(lessonEditorExecutor);

			clearLessonEditor(lessonEditorExecutor);
			break;
		case 2:
			lesson = getElement(mainSettings->lessons, listExecutor->selectedRow);

			delLesson(lesson);
			break;
		default:
			return res;
	}

	updateLessonManagerListExecutor(listExecutor);

	setFlag(&res, IH_CALL_UPDATE);
	
	return res;
}

FlagCarrier lessonManagerHandler(LessonManagerExecutor* executor)
{
	FlagCarrier res =  initFlagCarrier();
	LessonManager* lessonManager = (LessonManager*)getTemplate(executor);
	int readChar = getLastReadChar(executor);

	
	setFlag(&res,  IH_OK);
	
	if(readChar == KEY_F(5))
	{
		updateLessonManagerListExecutor(executor->lessonListExecutor);

		setFlag(&res, IH_CALL_UPDATE);
	}
	else if(readChar == KEY_LEFT || readChar == KEY_RIGHT || readChar == KEY_ENTER || readChar == '\n')
	{
		if(!getFlag(callInputHandler(executor->optionsChartExecutor, readChar), IH_OK))
		{
			zeroFlag(&res,  IH_OK);
		}
	}
	else if(readChar == KEY_UP || readChar == KEY_DOWN)
	{
		if(!getFlag(callInputHandler(executor->lessonListExecutor, readChar), IH_OK))
		{
			zeroFlag(&res,  IH_OK);
		}
	}

	return res;
}

void updateLessonManagerList(List* list)
{
	int i;
	
	PtrEntry* lessonList = mainSettings->lessons;

	if(lessonList == NULL)
	{
		list->rowsLength = 0;

		return;
	}

	for(i = 0; i < MAX_ARRAY_SIZE && lessonList->nextEntry != NULL; i++)
	{
		Lesson* l = (Lesson*)(lessonList->data);

		list->rows[i] = l == NULL ? "NULLIFIED" : l->lessonName;

		lessonList = nextEntry(lessonList);
	}
	
	for(; i < list->rowsLength; i++)
	{
		list->rows[i] = 0;
	}

	list->rowsLength = i;
}

void updateLessonManagerListExecutor(ListExecutor* listExecutor)
{
	updateLessonManagerList((List*)getTemplate(listExecutor));

	listExecutor->selectedRow = 0;
}

LessonManager* initLessonManager()
{
	LessonManager* res = (LessonManager*)malloc(sizeof(LessonManager));
	initTemplateAt(&res->temp, false, (PrintFunc)printLessonManager, emptyUpdateFunc, (InputHandler)lessonManagerHandler, NULL);
	
	res->lessonList = initList(NULL, MAX_ARRAY_SIZE);
	res->lessonList->rowsLength = 0;
	
	res->optionsChart = initChart((ClickHandler)chartClickHandler, 3, 1);
	res->optionsChart->data[0][0] = "Dodaj przedmiot";
	res->optionsChart->data[1][0] = "Modyfikuj przedmiot";
	res->optionsChart->data[2][0] = "Usun przedmiot";
	res->optionsChart->delimiter = ' ';
	

	return res;
}

LessonManagerExecutor* initLessonManagerExecutor(LessonManager* lessonManager, WINDOW* win)
{
	WINDOW* buffWin;

	LessonManagerExecutor* res = (LessonManagerExecutor*)malloc(sizeof(LessonManagerExecutor));
	initExecutorAt(&res->executor, &lessonManager->temp, win);

	buffWin = derwin(win, getMaxWinY(res) - 3, getMaxWinX(res), 1, 0);
	res->lessonListExecutor = initListExecutor(lessonManager->lessonList, buffWin);
	setSuperExecutor(res->lessonListExecutor, res);

	buffWin = derwin(win, 1, getMaxWinX(res), getMaxWinY(res) - 1, 0);
	res->optionsChartExecutor = initChartExecutor(lessonManager->optionsChart, NULL, NULL, buffWin);
	setSuperExecutor(res->optionsChartExecutor, res);
	
	return res;
}

void printLessonManager(LessonManagerExecutor* lessonManagerExecutor)
{
	LessonManager* lessonManager = (LessonManager*)getTemplate(lessonManagerExecutor);

	printElement(lessonManagerExecutor->lessonListExecutor);
	printElement(lessonManagerExecutor->optionsChartExecutor);
}

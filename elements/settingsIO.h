#ifndef SETTINGSIO_H
#define SETTINGSIO_H

#define INPUT_MODE 0
#define OUTPUT_MODE 1


#include <stdarg.h>

#include "../elementTemplates/element.h"
#include "../elementTemplates/menu.h"
#include "../structs.h"


extern Settings* mainSettings;


struct SettingsIO
{
	Template temp;

	InputBox* pathInputBox;
	
	char mode;
};
typedef struct SettingsIO SettingsIO;

struct SettingsIOExecutor
{
	Executor executor;

	InputBoxExecutor* pathInputBoxExecutor;

	char printBadFile;
};
typedef struct SettingsIOExecutor SettingsIOExecutor;

void initSettingsIOAt(SettingsIO* res, char mode);
SettingsIO* initSettingsIO(char mode);

void initSettingsIOExecutorAt(SettingsIOExecutor* res, SettingsIO* settingsIO, WINDOW* win);
SettingsIOExecutor* initSettingsIOExecutor(SettingsIO* settingsIO, WINDOW* win);

void printSettingsIO(SettingsIOExecutor* executor);

#endif

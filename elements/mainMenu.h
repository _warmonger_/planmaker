#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include <stdio.h>

#include "../structs.h"
#include "../elementTemplates/menu.h"
#include "../elementTemplates/config.h"
#include "../elements/help.h"
#include "../elements/mainConfig.h"

#include "../elements/settingsIO.h"
#include "../elements/planGeneration.h"
#include "../elements/manageLessons.h"

extern Settings* mainSettings;

void _initMainMenu();

FlagCarrier standardHandler(MenuExecutor* menuExecutor);

Menu* prepareMainMenu();
MenuExecutor* prepareMainMenuExecutor(Menu* m, WINDOW* win);

#endif

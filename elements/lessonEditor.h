#ifndef LESSONEDITOR_H
#define LESSONEDITOR_H

#define LESSON_EDITOR_SUBELEMENTS 4


#include <stdio.h>

#include "../structs.h"
#include "../elementTemplates/menu.h"
#include "../elementTemplates/config.h"
#include "../elementTemplates/chart.h"
#include "../elementTemplates/planChart.h"
#include "../elements/help.h"
#include "../elements/mainConfig.h"
#include "../elementTemplates/checkBox.h"


extern Settings* mainSettings;


struct LessonEditor;
typedef struct LessonEditor LessonEditor;

struct LessonEditorExecutor;
typedef struct LessonEditorExecutor LessonEditorExecutor;

struct LessonEditor
{
	Template temp;
	
	PlanChart* planChart;
	InputBox* varCodeInputBox;
	InputBox* lessonNameInputBox;
	CheckBox* isRequiredCheckBox;
};

struct LessonEditorExecutor
{
	Executor executor;
	
	int startY;
	int startX;

        int currentState;
	
	ChartExecutor* planChartExecutor;
	InputBoxExecutor* varCodeInputBoxExecutor;
	InputBoxExecutor* lessonNameInputBoxExecutor;
	CheckBoxExecutor* isRequiredCheckBoxExecutor;
	
	Lesson* currLesson;
};

void _initLessonEditor();

void loadLesson(LessonEditorExecutor* editor, Lesson* l);
Lesson* extractLesson(LessonEditorExecutor* editor);

void clearLessonEditor(LessonEditorExecutor* editor);

void initLessonEditorAt(LessonEditor* res);
LessonEditor* initLessonEditor();

void initLessonEditorExecutorAt(LessonEditorExecutor* res, LessonEditor* lessonEditor, WINDOW* win);
LessonEditorExecutor* initLessonEditorExecutor(LessonEditor* lessonEditor,  WINDOW* win);

Executor* getLessonEditorElement(LessonEditorExecutor* executor, int index);
Executor* getSelectedLessonEditorElement(LessonEditorExecutor* executor);

int getSelectedElementIndex(LessonEditorExecutor* executor);
void setSelectedElementIndex(LessonEditorExecutor* executor, int selIndex);

void printLessonEditor(LessonEditorExecutor* executor);
FlagCarrier lessonEditorInputHandler(LessonEditorExecutor* executor);

FlagCarrier lEChartClickHandler(ChartExecutor* executor);

#endif

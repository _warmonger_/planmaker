#ifndef MANAGELESSONS_H
#define MANAGELESSONS_H

#include <stdarg.h>

#include "../elements/lessonEditor.h"
#include "../elementTemplates/element.h"
#include "../elementTemplates/menu.h"
#include "../elementTemplates/chart.h"
#include "../structs.h"
#include "../ptrList.h"


extern Settings* mainSettings;


struct LessonManager
{
	Template temp;

	List* lessonList;
	Chart* optionsChart;
};
typedef struct LessonManager LessonManager;

struct LessonManagerExecutor
{
	Executor executor;
	
	ListExecutor* lessonListExecutor;
	ChartExecutor* optionsChartExecutor;
};
typedef struct LessonManagerExecutor LessonManagerExecutor;


void _initLessonManager();

LessonManager* initLessonManager();
LessonManagerExecutor* initLessonManagerExecutor(LessonManager* lessonManager, WINDOW* win);

void printLessonManager(LessonManagerExecutor* lessonManagerExecutor);

#endif

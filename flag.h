#ifndef FLAG_H
#define FLAG_H

/* Jesli 0 to liczba bitow flagi nieokreslona */
#define MAX_FLAG_BIT 63


#include <stdlib.h>

#define IH_OK 63


typedef long int FlagCarrier;


FlagCarrier* initFlagCarrier();

char getFlag(FlagCarrier flagCarrier, int bitNumber);
char setFlag(FlagCarrier* flagCarrier, int bitNumber);

char zeroFlag(FlagCarrier* flagCarrier, int bitNumber);
char flipFlag(FlagCarrier* flagCarrier, int bitNumber);

#endif

#include "ptrList.h"

PtrEntry* initPtrEntry(void* data)
{
	PtrEntry* res = (PtrEntry*)malloc(sizeof(PtrEntry));

	initPtrEntryAt(res, data);

	return res;
}

void initPtrEntryAt(PtrEntry* res, void* data)
{
	res->nextEntry = NULL;
	res->data = data;
}

int getListLength(PtrEntry* list)
{
	int i;

	if(list == NULL) { return -1; }

	for(i = 0; (list = nextEntry(list)) != NULL; i++);

	return i;
}

PtrEntry* nextEntry(PtrEntry* en)
{
	return en == NULL ? NULL : en->nextEntry;
}

PtrEntry* getElement(PtrEntry* list, int index)
{
	int i;

	if(index < 0) { return NULL; }

	for(i = 0; i < index; i++)
	{
		list = nextEntry(list);
	}

	return list;
}

void deleteElement(PtrEntry* list, int index)
{
	PtrEntry* prevEntry = getElement(list, index - 1);
	PtrEntry* enToDel = getElement(list, index);
	PtrEntry* nxEntry = nextEntry(enToDel);

	if(prevEntry == NULL) { return; }

	prevEntry->nextEntry = nxEntry;


}

void insertElement(PtrEntry* list, int index, PtrEntry* newEntry)
{
	PtrEntry* prevEntry = getElement(list, index - 1);
	PtrEntry* enToSet = getElement(list, index);

	newEntry->nextEntry = enToSet;
	if(prevEntry == NULL) { return; }

	prevEntry->nextEntry = newEntry;
}

PtrEntry* lastElement(PtrEntry* list)
{
	if(list == NULL) { return NULL; }
	if(list->nextEntry == NULL) { return list; }

	do
	{
		list = nextEntry(list);
	}
	while(list->nextEntry != NULL);

	return list;
}

Under development

Program written in C ANSI, designed for arranging study schedule. It allows to enter data (by using console UI created with ncurses library) about each course, its group types, whether it is required etc.
Based on this data, it will generate all possible schedules and will let user browse them starting from the one with the smallest amount of collisions.

Email: michal.s.jagodzinski@gmail.com
#include <stdio.h>
#include <stdlib.h>
#include <curses.h>

#include "elements/mainMenu.h"
#include "elements/manageLessons.h"
#include "elementTemplates/menu.h"
#include "structs.h"

Settings* mainSettings = NULL;
extern Menu* mainMenu;

Menu* mainMenu = NULL;
MenuExecutor* mainMenuExecutor = NULL;

extern HelpExecutor* helpExecutor;


void _init0x0()
{
	initscr();

	if(has_colors())
	{
		start_color();

		init_pair(1, COLOR_RED, COLOR_BLACK);
		init_pair(2, COLOR_WHITE, COLOR_BLACK);
		init_pair(3, COLOR_BLACK, COLOR_WHITE);
		init_pair(4, COLOR_CYAN, COLOR_BLACK);
	}

	mainSettings = initSettings(MAX_ARRAY_SIZE);

	mainMenu = prepareMainMenu();	
	mainMenuExecutor = prepareMainMenuExecutor(mainMenu, stdscr);

	_initMainMenu();
	_initLessonManager();

	noecho();
	keypad(stdscr, TRUE);
	raw();
}

int main(int len, const char** args)
{
	_init0x0();
/*InputBox* t = initInputBox("test");
	InputBoxExecutor* tE = initInputBoxExecutor(t, stdscr);

	execute(tE);*/
	if(len > 1 && (strcmp(args[0], "--help") || strcmp(args[0], "-h")))
	{
		execute(helpExecutor);

		endwin();
		exit(0);
	}

	Menu* mainMenu = prepareMainMenu();
	MenuExecutor* mainMenuExecutor = prepareMainMenuExecutor(mainMenu, stdscr);
	execute(mainMenuExecutor);

	endwin();

	return 0;
	
}

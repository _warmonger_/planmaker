# Michal Mortka

# Rezultatem kompletnego wykonania tego Makefile jest plik wykonywalny
# plan

CC=gcc
CCFLAGS=-ansi -pedantic -g
LIBS=-lcurses -lncurses -fPIC

all: plan semi-clean

rb: full-clean all

plan: main.o calc.o flag.o input.o ptrList.o structs.o elements.a elementTemplates.a
	$(CC) $(CCFLAGS) main.o calc.o flag.o input.o ptrList.o structs.o elements/elements.a elementTemplates/elementTemplates.a -lm -o plan $(LIBS)

elementTemplates.a: elementTemplates/.
	cd elementTemplates; make

elements.a: elementTemplates.a elements/.
	cd elements; make

calc.o:
	$(CC) $(CCFLAGS) -c calc.c
	
flag.o:
	$(CC) $(CCFLAGS) -c flag.c

input.o:
	$(CC) $(CCFLAGS) -c input.c

main.o:
	$(CC) $(CCFLAGS) -c main.c

mainMenu.o:
	$(CC) $(CCFLAGS) -c mainMenu.c
	
ptrList.o:
	$(CC) $(CCFLAGS) -c ptrList.c

structs.o:
	$(CC) $(CCFLAGS) -c structs.c

clean: semi-clean full-clean

semi-clean:
	rm -f *.o *.a *.so *~
	cd elementTemplates; make clean
	cd elements; make clean

full-clean: semi-clean
	rm -f plan

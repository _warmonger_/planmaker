#ifndef CALC_H
#define CALC_H

#include <string.h>
#include <stdlib.h>

#include "structs.h"


void cloneAt(Match* res, Match* match);
Match* clone(Match* match);

void pushLesson(Match* m, Lesson* l, int variantIndex);
void purgeVariant(Match* m, int variantIndex);

void computeMatches(Lesson** lessons, int lessonLength, int currentLessonIndex, Match* matches, int maxMatchesLength);

void compute(Lesson** lessons, int lessonLength, int currentLessonIndex, Match* matches, int maxMatchesLength, int* currentMatchIndex_);

#endif

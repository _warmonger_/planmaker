/* Michal Mortka */

#include "input.h"

/* Jesli handler == NULL to wyjscie z petli nastapi po podaniu prawidlowego ciagu znakow,
 * konwertowalnego na dany typ zmiennej. Jesli handler =/= NULL, wtedy po otrzymaniu
 * i prawidlowym przekonwertowaniu ciagu znakow na zmienna liczbowa wywolywana jest
 * funkcja handler(otrzymana_zmienna) - jesli zwroci prawde, program wychodzi z petli,
 * jesli nie, znaczy to ze warunki zewnetrzne nie sa spelnione i pobranie danej trzeba
 * powtorzyc */

void w_czytajIntHV(WINDOW* win, const char* desc, int* var, char (*handler)(int*, int, void**), int len, void** args)
{
	char hardHandler(int* v)
	{
		return handler(v, len, args);
	}

	return w_czytajIntH(win, desc, var, hardHandler);
}

void czytajIntHV(const char* desc, int* var, char (*handler)(int*, int, void**), int len, void** args)
{
	char hardHandler(int* v)
	{
		return handler(v, len, args);
	}

	return czytajIntH(desc, var, hardHandler);
}

void czytajDoubleHV(const char* desc, double* var, char (*handler)(double*, int, void**), int len, void** args)
{
	char hardHandler(double* v)
	{
		return handler(v, len, args);
	}

	return czytajDoubleH(desc, var, hardHandler);
}

void czytajHV(const char* desc, const char* scanwCode, const char* error, void* var, char (*handler)(void*, int, void**), int len, void** args)
{
	char hardHandler(void* v)
	{
		return handler(v, len, args);
	}

	return czytajH(desc, scanwCode, error, var, hardHandler);
}





void w_czytajIntH(WINDOW* win, const char* desc, int* var, char (*handler)(int*))
{
	int currY, currX;

	wprintw(win, "%s", desc);
	getyx(win, currY, currX);

	do
	{
		wmove(win, currY, currX);
		wclrtoeol(win);

		int res = wscanw(win, "%i", var);
		wclrtobot(win);
		
        	if(res < 1)
        	{
                	wprintw(win, "Nieprawidlowa wartosc, podaj liczbe calkowita!");

			wrefresh(win);

			continue;
		}

		wrefresh(win);

		if(!(handler != NULL && !handler(var)))
		{
			break;
		}
	}
	while(1);
}

void czytajIntH(const char* desc, int* var, char (*handler)(int*))
{
	do
	{
		printw("%s", desc);

        	while(scanw("%i", var) < 1)
        	{
                	printw("Nieprawidlowa wartosc, podaj liczbe calkowita!\n%s", desc);

                	__fpurge(stdin);
		}
	}
	while(handler != NULL && !handler(var));
}

void czytajDoubleH(const char* desc, double* var, char (*handler)(double*))
{
        do  
        {
		printw("%s", desc);

                while(scanw("%lf", var) < 1)
                {
                        printw("Nieprawidlowa wartosc, podaj liczbe!\n%s", desc);

                        __fpurge(stdin);
                }
        } 
        while(handler != NULL && !handler(var));
}

void czytajH(const char* desc, const char* scanwCode, const char* error, void* var, char (*handler)(void*))
{
        do  
        {
		printw("%s", desc);

                while(scanw(scanwCode, var) < 1)
                {
                        printw("%s\n%s", error, desc);

                        __fpurge(stdin);
                }
        } 
        while(handler != NULL && !handler(var));
}



void czytajInt(const char* desc, int* var)
{
	czytajIntH(desc, var, NULL);
}

void czytajDouble(const char* desc, double* var)
{
	czytajDoubleH(desc, var, NULL);
}

void czytaj(const char* desc, const char* scanwCode, const char* error, void* var)
{
	czytajH(desc, scanwCode, error, var, NULL);
}





/*void czytajStringH(const char* desc, char* var, char (*handler)(const char*))
{
	do  
        {
		printw("Podaj %s: ", desc);

                while(scanw(scanwCode, var) < 1)
                {
                        printw("Nieprawidlowy ciag znakow (?)!\nPodaj %s: ", desc);

                        __fpurge(stdin);
                }
        } 
        while(handler != NULL && !handler(var));
}

void czytajString(const char* desc, char* var)
{
	czytajStringH(desc, var, NULL);
}*/

const char* czytajStringHV(const char* desc, char (*handler)(const char*, int, void**), int len, void** args)
{
	char hardHandler(const char* c)
	{
		return handler(c, len, args);
	}

	return czytajStringH(desc, hardHandler);
}

const char* czytajStringH(const char* desc, char (*handler)(const char*))
{
	char* var = malloc(128);
	
	do  
        {
		printw("%s", desc);

                while(scanw("%s", var) < 1)
                {
                        printw("Nieprawidlowy ciag znakow (?)!\n%s", desc);

                        __fpurge(stdin);
                }
        } 
        while(handler != NULL && !handler(var));

	char* res = malloc(strlen(var));

	memcpy(res, var, strlen(var));

	free(var);

	return (const char*)res;
}

const char* czytajString(const char* desc)
{
	return czytajStringH(desc, NULL);
}

#ifndef STRUCTS_H
#define STRUCTS_H

#define PERIODS_PER_VARIANT 5
#define VARIANTS_PER_LESSON 5
#define VARIANTS_PER_MATCH 5

#define MAX_ARRAY_SIZE 128

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>

#include "ptrList.h"

struct Time
{
	int dayNumber;
	int hour;
	int minute;
};
typedef struct Time Time;


struct Period
{
	Time start;
	Time end;
};
typedef struct Period Period;


struct Variant
{
	char* variantCode;
	
	int periodsLength;
	Period* periods;
};
typedef struct Variant Variant;


struct Lesson
{
	char* lessonName;
	
	int variantsLength;
	Variant* variants;
};
typedef struct Lesson Lesson;


struct VariantData
{
	Lesson* lesson;
	int variantIndex;
};
typedef struct VariantData VariantData;


struct Match
{
	int vDsLength;
	VariantData* variantDatas;
};
typedef struct Match Match;

struct Settings
{
	int lessonsLength;
	PtrEntry* lessons;

	int maxCollisionLength;		/* MINUTES */
	int maxCollisionAmount;
	int maxGeneratedPlanAmount;

	Time startTime;			/* HOUR,MINUTES */
	int periodLength;		/* MINUTES */
	int periodsAmount;
};
typedef struct Settings Settings;



const char* timeToString(Time* t);

int getDayCode(char* str);
int doesCollide(Period* p0, Period* p1);

int timeToInt(Time* t);
Time* intToTime(int i);

void increaseTimeBy(Time* origin, Time* by);
Time* timeByTime(int dayNumber, int hour, int minute);


Time* initTime();
Time* readTime(FILE* f);
char readTimeInto(FILE* f, Time* res);
char writeTime(FILE* f, Time* t);

Period* initPeriod();
Period* readPeriod(FILE* f);
char readPeriodInto(FILE* f, Period* res);
char writePeriod(FILE* f, Period* p);

void initVariantAt(Variant* v);
Variant* initVariant();
Variant* readVariant(FILE* f);
char readVariantInto(FILE* f, Variant* res);
char writeVariant(FILE* f, Variant* v);

Lesson* initLesson();
Lesson* readLesson(FILE* f);
char readLessonInto(FILE* f, Lesson* l);
PtrEntry* readLessonPack(FILE* f, int* length);
char writeLesson(FILE* f, Lesson* l);
char writeLessonPack(FILE* f, PtrEntry* ls);


Settings* initSettings();
Settings* readSettings(FILE* f);
char readSettingsInto(FILE* f, Settings* res);
char writeSettings(FILE* f, Settings* l);

Match* initMatch(int maxVariantsLength);

char addLesson(Lesson* l);
char delLesson(int index);

Lesson** convertLessons();

#endif

/* Michal Mortka */

#ifndef INPUT_H
#define INPUT_H

#include <stdarg.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <ncurses.h>

struct MenuExecutor;

/* Naglowki funkcji interaktywnego wejscia danych (handler to zewnetrzna funkcja np.
 * sprawdzajaca inne warunki ktore musi spelniac wpisana wartosc), z mozliwoscia podania dodatkowych danych */
void w_czytajIntHV(WINDOW* win, const char* desc, int* var, char (*handler)(int*, int, void**), int len, void** args);
void czytajIntHV(const char* desc, int* var, char (*handler)(int*, int, void**), int len, void** args);
void czytajDoubleHV(const char* desc, double* var, char (*handler)(double*, int, void**), int len, void** args);
void czytajHV(const char* desc, const char* scanwCode, const char* error, void* var, char (*handler)(void*, int, void**), int len, void** args);


/* Naglowki funkcji interaktywnego wejscia danych (handler to zewnetrzna funkcja np.
 * sprawdzajaca inne warunki ktore musi spelniac wpisana wartosc) */
void w_czytajIntH(WINDOW* win, const char* desc, int* var, char (*handler)(int*));
void czytajIntH(const char* desc, int* var, char (*handler)(int*));
void czytajDoubleH(const char* desc, double* var, char (*handler)(double*));
void czytajH(const char* desc, const char* scanwCode, const char* error, void* var, char (*handler)(void*));

/* Naglowki "przeciazen" powyzszych funkcji z pominieciem handlera */
void czytajInt(const char* desc, int* var);
void czytajDouble(const char* desc, double* var);
void czytaj(const char* desc, const char* scanwCode, const char* error, void* var);

/* Aktualizacja o nowe funkcje */
const char* czytajStringHV(const char* desc, char (*handler)(const char*, int, void**), int len, void** args);
const char* czytajStringH(const char* desc, char (*handler)(const char*));
const char* czytajString(const char* desc);

#endif
